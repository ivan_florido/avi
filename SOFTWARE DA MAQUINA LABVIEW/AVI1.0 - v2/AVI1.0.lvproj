﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{018D9236-A23F-41AD-B00E-15135D32179A}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/S1_Flipper</Property>
	<Property Name="varPersistentID:{0355DE7E-E70C-44B0-B9BE-AFAA2614AB86}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Emerg4</Property>
	<Property Name="varPersistentID:{0B86A78A-219D-4D23-BBC5-ED012B5E546C}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100599</Property>
	<Property Name="varPersistentID:{10B09D33-D5DB-4C89-8519-024A26C609E4}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Rst_Rob</Property>
	<Property Name="varPersistentID:{13B8DFB7-E52A-4F94-AADE-3F6E9C87EA54}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Sensor_Vacf</Property>
	<Property Name="varPersistentID:{1712A597-35D5-4F10-8922-B930CA85D1D8}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Verde_sai</Property>
	<Property Name="varPersistentID:{1E594C26-FFF7-4533-9003-5A45E945A7B9}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100613</Property>
	<Property Name="varPersistentID:{201C5E58-AF34-49D5-ABF4-F7A94B481FC7}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Sensor_Reject</Property>
	<Property Name="varPersistentID:{267EE46E-DDFF-4731-ADA9-4EB057055078}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100597</Property>
	<Property Name="varPersistentID:{2BDB2579-B4E4-496E-B860-50C050D769DB}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100630</Property>
	<Property Name="varPersistentID:{2E60A9B8-F6EB-4B64-9C67-06824EB7A76E}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/R_Emerg</Property>
	<Property Name="varPersistentID:{3759C221-93EF-4516-8DA7-19379C373DCA}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Reset_Sec</Property>
	<Property Name="varPersistentID:{3F8D5888-726B-4949-97C9-03C5716F49C4}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/R_Ready</Property>
	<Property Name="varPersistentID:{40750F19-58AF-419C-AD12-CEA765BEE38E}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/R_Run</Property>
	<Property Name="varPersistentID:{419F1C8B-B70A-43D1-B228-E03AF7CA0B0B}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Sensor_Out</Property>
	<Property Name="varPersistentID:{477C294A-5DF5-4EDC-9A62-B7205933D204}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/R_Warning</Property>
	<Property Name="varPersistentID:{54DAE6FF-FE97-4317-B402-1624943E533B}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100610</Property>
	<Property Name="varPersistentID:{573AB499-3C66-4A7B-96CC-0E99DA4D730A}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Sensor1_Vacg</Property>
	<Property Name="varPersistentID:{5F72D8EF-1964-416A-A20C-0C37840FE7AE}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Verde_ent</Property>
	<Property Name="varPersistentID:{618AA3F0-C24D-489F-AFF3-A9BCC624991B}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Conveyor</Property>
	<Property Name="varPersistentID:{623714CE-F402-4A54-8521-E2B58B8C5099}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Amarelo_sai</Property>
	<Property Name="varPersistentID:{626B86FD-D5CE-4FBE-B954-E42AD8AF44FB}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100611</Property>
	<Property Name="varPersistentID:{65F85CFA-670D-4AA8-BA7A-C18A4D51E701}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/000522</Property>
	<Property Name="varPersistentID:{664A6C0F-1370-4A28-85B9-8E1DC1A11D0E}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Vacc_gant</Property>
	<Property Name="varPersistentID:{66B90970-8D76-4B03-82EE-CA5EC0004464}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Emerg2</Property>
	<Property Name="varPersistentID:{6770E1EF-F818-4F88-A7ED-6EAA07D07F6B}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100612</Property>
	<Property Name="varPersistentID:{702C05D4-E9D9-4BB6-8AD3-5AD107FA8E36}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Vaccu_Flip</Property>
	<Property Name="varPersistentID:{729368BA-B6D8-489E-9DD9-A313ADC0BD54}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Flipper</Property>
	<Property Name="varPersistentID:{77C0D430-4FEA-4428-AF32-040442A75E4E}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Sensor2_Vacg</Property>
	<Property Name="varPersistentID:{8715F77A-B63B-4BE7-A9E2-A4B6C9460104}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100631</Property>
	<Property Name="varPersistentID:{8C140120-5658-4E9E-BDD2-5848A97A50E9}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/000527</Property>
	<Property Name="varPersistentID:{96B92EED-4A25-4AAE-B0D3-F9EFEFCE8D08}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/000523</Property>
	<Property Name="varPersistentID:{9CA5F413-0D1B-46D7-A599-75387C8C611B}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Vermelho</Property>
	<Property Name="varPersistentID:{9E374B6D-D333-42CB-943B-9E81D8891F66}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Signal_Sec</Property>
	<Property Name="varPersistentID:{9FA5A404-5838-45B5-AB78-6996F67CEB87}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/R_Error</Property>
	<Property Name="varPersistentID:{A2315F2D-B48B-4320-8F6C-7B53A9B12D28}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100614</Property>
	<Property Name="varPersistentID:{B1706C05-31E3-4F2C-82A9-864D48D6D0DE}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Sensor_Press</Property>
	<Property Name="varPersistentID:{B487C633-12C9-4F94-A59A-9703CCDCCCA3}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100623</Property>
	<Property Name="varPersistentID:{BF0913E4-3B00-49E7-8199-7B730115A808}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/S2_Flipper</Property>
	<Property Name="varPersistentID:{C2DF48B8-64B3-4DC3-87E8-1082891564F8}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100629</Property>
	<Property Name="varPersistentID:{C80AD983-333E-4FAB-BC06-806019DE7EE4}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Emerg3</Property>
	<Property Name="varPersistentID:{D059D246-B884-4CA4-8F6B-9752B49C8E1C}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Stop_Rob</Property>
	<Property Name="varPersistentID:{D576AB2B-6B64-4E98-8847-9EDCEC5A8812}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Sensor_Berco</Property>
	<Property Name="varPersistentID:{D6F81EEC-33C8-4B2D-8709-5B6BD75FE5AA}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Sensor_Ent</Property>
	<Property Name="varPersistentID:{E0935D9B-40DF-4333-AF2C-280B7D73A8C2}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Amarelo_ent</Property>
	<Property Name="varPersistentID:{E3D1EF84-90FD-4EA8-A78B-9F8C4A746671}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/000521</Property>
	<Property Name="varPersistentID:{E8385A39-B8D5-4FC6-BB3D-F44ABB658696}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Illumination</Property>
	<Property Name="varPersistentID:{EA006C26-EE6E-4D89-A807-DF2347856A08}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/100598</Property>
	<Property Name="varPersistentID:{F1CFBE49-9B2B-48E0-8285-0C79E864ABF5}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Sensor1_Conveyor</Property>
	<Property Name="varPersistentID:{F32C7546-4090-4824-9333-4107BD1DE1EE}" Type="Ref">/My Computer/ModbusIO_AVI.lvlib/Emerg1</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="mains" Type="Folder">
			<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
		</Item>
		<Item Name="Project Documentation" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Documentation Images" Type="Folder">
				<Property Name="NI.SortType" Type="Int">0</Property>
				<Item Name="loc_access_task_data.png" Type="Document" URL="../documentation/loc_access_task_data.png"/>
				<Item Name="loc_bundle_new_button_ref.png" Type="Document" URL="../documentation/loc_bundle_new_button_ref.png"/>
				<Item Name="loc_convert_variant.png" Type="Document" URL="../documentation/loc_convert_variant.png"/>
				<Item Name="loc_create_two_queues.png" Type="Document" URL="../documentation/loc_create_two_queues.png"/>
				<Item Name="loc_disable_new_button.png" Type="Document" URL="../documentation/loc_disable_new_button.png"/>
				<Item Name="loc_enqueue_generic_message.png" Type="Document" URL="../documentation/loc_enqueue_generic_message.png"/>
				<Item Name="loc_enqueue_message_with_data.png" Type="Document" URL="../documentation/loc_enqueue_message_with_data.png"/>
				<Item Name="loc_enqueue_priority_message.png" Type="Document" URL="../documentation/loc_enqueue_priority_message.png"/>
				<Item Name="loc_exit_message.png" Type="Document" URL="../documentation/loc_exit_message.png"/>
				<Item Name="loc_message_queue_wire.png" Type="Document" URL="../documentation/loc_message_queue_wire.png"/>
				<Item Name="loc_new_message_diagram.png" Type="Document" URL="../documentation/loc_new_message_diagram.png"/>
				<Item Name="loc_new_task_loop.png" Type="Document" URL="../documentation/loc_new_task_loop.png"/>
				<Item Name="loc_new_task_typedef.png" Type="Document" URL="../documentation/loc_new_task_typedef.png"/>
				<Item Name="loc_open_msg_queue_typedef.png" Type="Document" URL="../documentation/loc_open_msg_queue_typedef.png"/>
				<Item Name="loc_qmh_ignore_errors.png" Type="Document" URL="../documentation/loc_qmh_ignore_errors.png"/>
				<Item Name="loc_queued_message_handler.gif" Type="Document" URL="../documentation/loc_queued_message_handler.gif"/>
				<Item Name="loc_stop_new_mhl.png" Type="Document" URL="../documentation/loc_stop_new_mhl.png"/>
				<Item Name="loc_stop_task.png" Type="Document" URL="../documentation/loc_stop_task.png"/>
				<Item Name="loc_ui_data.png" Type="Document" URL="../documentation/loc_ui_data.png"/>
				<Item Name="loc_value_change_event.png" Type="Document" URL="../documentation/loc_value_change_event.png"/>
				<Item Name="noloc_note.png" Type="Document" URL="../documentation/noloc_note.png"/>
				<Item Name="noloc_tip.png" Type="Document" URL="../documentation/noloc_tip.png"/>
			</Item>
			<Item Name="Queued Message Handler Documentation.html" Type="Document" URL="../documentation/Queued Message Handler Documentation.html"/>
		</Item>
		<Item Name="Support VIs" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Message Queue.lvlib" Type="Library" URL="../support/Message Queue/Message Queue.lvlib"/>
			<Item Name="User Event - Stop.lvlib" Type="Library" URL="../support/User Event - Stop/User Event - Stop.lvlib"/>
			<Item Name="Check Loop Error.vi" Type="VI" URL="../support/Check Loop Error.vi"/>
			<Item Name="Error Handler - Event Handling Loop.vi" Type="VI" URL="../support/Error Handler - Event Handling Loop.vi"/>
			<Item Name="Error Handler - Message Handling Loop.vi" Type="VI" URL="../support/Error Handler - Message Handling Loop.vi"/>
			<Item Name="Operations_In.vi" Type="VI" URL="../support/Operations_In.vi"/>
			<Item Name="Pulse_out_robot.vi" Type="VI" URL="../support/Pulse_out_robot.vi"/>
			<Item Name="lista_home_auto.vi" Type="VI" URL="../support/lista_home_auto.vi"/>
			<Item Name="VI_troca_rede_cognex.vi" Type="VI" URL="../support/VI_troca_rede_cognex.vi"/>
			<Item Name="grab_subvi2.vi" Type="VI" URL="../support/grab_subvi2.vi"/>
			<Item Name="log_rcolocarcel.vi" Type="VI" URL="../support/log_rcolocarcel.vi"/>
			<Item Name="cqa_ff_getunitinfo_edu.vi" Type="VI" URL="../support/cqa_ff_getunitinfo_edu.vi"/>
			<Item Name="register_product.vi" Type="VI" URL="../support/register_product.vi"/>
			<Item Name="refresh_reject.vi" Type="VI" URL="../support/refresh_reject.vi"/>
			<Item Name="tid_camloop.vi" Type="VI" URL="../support/tid_camloop.vi"/>
			<Item Name="models.vi" Type="VI" URL="../support/models.vi"/>
			<Item Name="save_image.vi" Type="VI" URL="../support/save_image.vi"/>
			<Item Name="qtd_fotos.vi" Type="VI" URL="../support/qtd_fotos.vi"/>
			<Item Name="grab_subvi.vi" Type="VI" URL="../support/grab_subvi.vi"/>
			<Item Name="ftp_sendim_log.vi" Type="VI" URL="../support/ftp_sendim_log.vi"/>
			<Item Name="generate_filename_2.vi" Type="VI" URL="../support/generate_filename_2.vi"/>
			<Item Name="send_name_subvi.vi" Type="VI" URL="../support/send_name_subvi.vi"/>
			<Item Name="COGNEX_IA_LINK.vi" Type="VI" URL="../support/COGNEX_IA_LINK.vi"/>
			<Item Name="global_result.vi" Type="VI" URL="../support/global_result.vi"/>
			<Item Name="log_cycle_time.vi" Type="VI" URL="../support/log_cycle_time.vi"/>
			<Item Name="cut_image.vi" Type="VI" URL="../support/cut_image.vi"/>
			<Item Name="cqa_ff_addtest_v2.vi" Type="VI" URL="../support/cqa_ff_addtest_v2.vi"/>
			<Item Name="send_log.vi" Type="VI" URL="../support/send_log.vi"/>
			<Item Name="init_io.vi" Type="VI" URL="../support/init_io.vi"/>
			<Item Name="home_subvi.vi" Type="VI" URL="../support/home_subvi.vi"/>
			<Item Name="tempoexposicao.vi" Type="VI" URL="../support/tempoexposicao.vi"/>
			<Item Name="timeout_tempo_expo.vi" Type="VI" URL="../support/timeout_tempo_expo.vi"/>
			<Item Name="check_sensor.vi" Type="VI" URL="../support/check_sensor.vi"/>
			<Item Name="save_image_x.vi" Type="VI" URL="../support/save_image_x.vi"/>
			<Item Name="elastic_post_error_info.vi" Type="VI" URL="../support/elastic_post_error_info.vi"/>
			<Item Name="elastic_post_img_info.vi" Type="VI" URL="../support/elastic_post_img_info.vi"/>
			<Item Name="elastic_post_troca_rede.vi" Type="VI" URL="../support/elastic_post_troca_rede.vi"/>
			<Item Name="elastic_post_uid_info.vi" Type="VI" URL="../support/elastic_post_uid_info.vi"/>
			<Item Name="save_bmp.vi" Type="VI" URL="../support/save_bmp.vi"/>
			<Item Name="error_log_file.vi" Type="VI" URL="../support/error_log_file.vi"/>
			<Item Name="delay_proc_subvi.vi" Type="VI" URL="../support/delay_proc_subvi.vi"/>
			<Item Name="entrada_delay_subvi.vi" Type="VI" URL="../support/entrada_delay_subvi.vi"/>
			<Item Name="interface_rejeito_cqa.vi" Type="VI" URL="../support/interface_rejeito_cqa.vi"/>
			<Item Name="result_ia_array.vi" Type="VI" URL="../support/result_ia_array.vi"/>
			<Item Name="select_out_reject.vi" Type="VI" URL="../support/select_out_reject.vi"/>
			<Item Name="error_cortina_subvi.vi" Type="VI" URL="../support/error_cortina_subvi.vi"/>
			<Item Name="rotate_subvi_label.vi" Type="VI" URL="../support/rotate_subvi_label.vi"/>
			<Item Name="conv_subvi.vi" Type="VI" URL="../support/conv_subvi.vi"/>
			<Item Name="conv_subvi2.vi" Type="VI" URL="../support/conv_subvi2.vi"/>
			<Item Name="Operations_In_conveyor.vi" Type="VI" URL="../support/Operations_In_conveyor.vi"/>
			<Item Name="timeout_tempo_gamma.vi" Type="VI" URL="../support/timeout_tempo_gamma.vi"/>
			<Item Name="verif_model.vi" Type="VI" URL="../support/verif_model.vi"/>
		</Item>
		<Item Name="Type Definitions" Type="Folder">
			<Item Name="Control ng.ctl" Type="VI" URL="../controls/Control ng.ctl"/>
			<Item Name="Control ok.ctl" Type="VI" URL="../controls/Control ok.ctl"/>
			<Item Name="enum_output.ctl" Type="VI" URL="../controls/enum_output.ctl"/>
			<Item Name="UI Data.ctl" Type="VI" URL="../controls/UI Data.ctl"/>
		</Item>
		<Item Name="camera_grid_2.vi" Type="VI" URL="../support/camera_grid_2.vi"/>
		<Item Name="Main_esteira.vi" Type="VI" URL="../Main_esteira.vi"/>
		<Item Name="ModbusIO_AVI.lvlib" Type="Library" URL="../ModbusIO_AVI.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Barcode Search Options.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Instrument.llb/Barcode Search Options.ctl"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Panel.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/victl.llb/Close Panel.vi"/>
				<Item Name="Color (U64)" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Color (U64)"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Create Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Create Semaphore.vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Destroy Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Destroy Semaphore.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="Edge Options.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Measure.llb/Edge Options.ctl"/>
				<Item Name="Edge Polarity.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Measure.llb/Edge Polarity.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Flatten Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pixmap.llb/Flatten Pixmap.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="FormatTime String.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/FormatTime String.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get Semaphore Status.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Get Semaphore Status.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="Grid Descriptor" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Grid Descriptor"/>
				<Item Name="Image Type" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Type"/>
				<Item Name="Image Unit" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Unit"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="IMAQ Clear Overlay" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Clear Overlay"/>
				<Item Name="IMAQ Coordinate System" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Coordinate System"/>
				<Item Name="IMAQ Copy" Type="VI" URL="/&lt;vilib&gt;/vision/Management.llb/IMAQ Copy"/>
				<Item Name="IMAQ Create" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Create"/>
				<Item Name="IMAQ Dispose" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Dispose"/>
				<Item Name="Imaq GetImageInfo" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/Imaq GetImageInfo"/>
				<Item Name="IMAQ GetImageSize" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ GetImageSize"/>
				<Item Name="IMAQ Image.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Image.ctl"/>
				<Item Name="IMAQ ImageToArray" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ ImageToArray"/>
				<Item Name="IMAQ Overlay Line" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay Line"/>
				<Item Name="IMAQ Overlay Rectangle" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay Rectangle"/>
				<Item Name="IMAQ ReadFile 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ ReadFile 2"/>
				<Item Name="IMAQ Rounding Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Rounding Mode.ctl"/>
				<Item Name="IMAQ SetImageSize" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ SetImageSize"/>
				<Item Name="IMAQ Write BMP File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write BMP File 2"/>
				<Item Name="IMAQ Write BMP String" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write BMP String"/>
				<Item Name="IMAQ Write File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write File 2"/>
				<Item Name="IMAQ Write Image And Vision Info File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write Image And Vision Info File 2"/>
				<Item Name="IMAQ Write Image And Vision Info String" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write Image And Vision Info String"/>
				<Item Name="IMAQ Write JPEG File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write JPEG File 2"/>
				<Item Name="IMAQ Write JPEG String" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write JPEG String"/>
				<Item Name="IMAQ Write JPEG2000 File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write JPEG2000 File 2"/>
				<Item Name="IMAQ Write PNG File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write PNG File 2"/>
				<Item Name="IMAQ Write PNG String" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write PNG String"/>
				<Item Name="IMAQ Write String" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write String"/>
				<Item Name="IMAQ Write TIFF File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write TIFF File 2"/>
				<Item Name="IMAQ Write TIFF String" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write TIFF String"/>
				<Item Name="IMAQdx.ctl" Type="VI" URL="/&lt;vilib&gt;/userdefined/High Color/IMAQdx.ctl"/>
				<Item Name="IVA Append VI Name to GUID.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Append VI Name to GUID.vi"/>
				<Item Name="IVA Barcode Format Code 3.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Barcode Format Code 3.vi"/>
				<Item Name="IVA Caliper - Strings.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Caliper - Strings.vi"/>
				<Item Name="IVA Caliper Algorithm.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Caliper Algorithm.vi"/>
				<Item Name="IVA Caliper Sub-VI 3.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Caliper Sub-VI 3.vi"/>
				<Item Name="IVA Check Coordinate System Valid 2.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Check Coordinate System Valid 2.vi"/>
				<Item Name="IVA Clear Coordsys Errors.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Clear Coordsys Errors.vi"/>
				<Item Name="IVA Coordinate System Manager 2.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Coordinate System Manager 2.vi"/>
				<Item Name="IVA Image Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Image Buffer.vi"/>
				<Item Name="IVA Mask from ROI.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Mask from ROI.vi"/>
				<Item Name="IVA Result Manager Function.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Result Manager Function.ctl"/>
				<Item Name="IVA Result Manager.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Result Manager.vi"/>
				<Item Name="IVA Store Caliper Results.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Store Caliper Results.vi"/>
				<Item Name="IVA Store Straight Edge3 Results.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Store Straight Edge3 Results.vi"/>
				<Item Name="IVA Unit2String.vi" Type="VI" URL="/&lt;vilib&gt;/vision/Vision Assistant Utils.llb/IVA Unit2String.vi"/>
				<Item Name="LabVIEWHTTPClient.lvlib" Type="Library" URL="/&lt;vilib&gt;/httpClient/LabVIEWHTTPClient.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_FTP.lvlib" Type="Library" URL="/&lt;vilib&gt;/FTP/NI_FTP.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_Vision_Acquisition_Software.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/driver/NI_Vision_Acquisition_Software.lvlib"/>
				<Item Name="NI_Vision_Development_Module.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/NI_Vision_Development_Module.lvlib"/>
				<Item Name="Normalize End Of Line.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Normalize End Of Line.vi"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Open Panel.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/victl.llb/Open Panel.vi"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="ROI Descriptor" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/ROI Descriptor"/>
				<Item Name="Semaphore Name &amp; Ref DB Action.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Name &amp; Ref DB Action.ctl"/>
				<Item Name="Semaphore Name &amp; Ref DB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Name &amp; Ref DB.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Straight Edge Options.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Measure.llb/Straight Edge Options.ctl"/>
				<Item Name="Straight Edge Process.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Measure.llb/Straight Edge Process.ctl"/>
				<Item Name="subElapsedTime.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/subElapsedTime.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="Vision Info Type" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Vision Info Type"/>
				<Item Name="Vision Info Type2.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Vision Info Type2.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
			</Item>
			<Item Name="cqa_ff_addtest.vi" Type="VI" URL="../support/cqa_ff_addtest.vi"/>
			<Item Name="ftp_send_log.vi" Type="VI" URL="../support/ftp_send_log.vi"/>
			<Item Name="ftp_sendim.vi" Type="VI" URL="../support/ftp_sendim.vi"/>
			<Item Name="marca_pos.vi" Type="VI" URL="../support/marca_pos.vi"/>
			<Item Name="niimaqdx.dll" Type="Document" URL="niimaqdx.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivision.dll" Type="Document" URL="nivision.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivissvc.dll" Type="Document" URL="nivissvc.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="readtxt.vi" Type="VI" URL="../support/readtxt.vi"/>
			<Item Name="send_image.vi" Type="VI" URL="../support/send_image.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="AVI1.0" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{B243632A-31F7-4360-9D21-CB23F52257AF}</Property>
				<Property Name="App_INI_GUID" Type="Str">{94E464F1-FCE9-4D95-BE57-95137D93D659}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">0</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{628B4197-DC83-4C53-92B1-D685AF622611}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">AVI1.0</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/AVI1.0</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{8C6B452C-732F-4240-88FF-79EB8E77371A}</Property>
				<Property Name="Bld_version.build" Type="Int">9</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">AVI1.0.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/AVI1.0/AVI1.0.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/AVI1.0/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_Vardep[0].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[0].LibItemID" Type="Ref">/My Computer/ModbusIO_AVI.lvlib</Property>
				<Property Name="Exe_VardepDeployAtStartup" Type="Bool">true</Property>
				<Property Name="Exe_VardepLibItemCount" Type="Int">1</Property>
				<Property Name="Source[0].itemID" Type="Str">{F4D92E35-4241-4D53-97BB-9195C7DE1DEF}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/mains/Main.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/ModbusIO_AVI.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">FIT</Property>
				<Property Name="TgtF_fileDescription" Type="Str">AVI1.0</Property>
				<Property Name="TgtF_internalName" Type="Str">AVI1.0</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2021 FIT</Property>
				<Property Name="TgtF_productName" Type="Str">AVI1.0</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{F38A68ED-A616-49ED-A8FF-273C3D9455DD}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">AVI1.0.exe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
			<Item Name="LB Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">AVI1.0</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{775A20AE-CFB2-4C64-9D05-7D288EB3285B}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{6B440D80-3B0D-43B2-8A06-E2E939AA1006}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI LabVIEW Runtime 2020 SP1</Property>
				<Property Name="DistPart[0].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[0].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[0].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[0].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[1].productName" Type="Str">NI Deployment Framework 2020</Property>
				<Property Name="DistPart[0].SoftDep[1].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[0].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[10].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[0].SoftDep[10].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[0].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[11].productName" Type="Str">NI TDM Streaming 19.0</Property>
				<Property Name="DistPart[0].SoftDep[11].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[0].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[2].productName" Type="Str">NI Error Reporting 2020</Property>
				<Property Name="DistPart[0].SoftDep[2].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[0].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[3].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2020</Property>
				<Property Name="DistPart[0].SoftDep[3].upgradeCode" Type="Str">{00D0B680-F876-4E42-A25F-52B65418C2A6}</Property>
				<Property Name="DistPart[0].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[4].productName" Type="Str">NI LabVIEW Runtime 2020 SP1 Non-English Support.</Property>
				<Property Name="DistPart[0].SoftDep[4].upgradeCode" Type="Str">{61FCC73D-8092-457D-8905-27C0060D88E1}</Property>
				<Property Name="DistPart[0].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[5].productName" Type="Str">NI Logos 20.0</Property>
				<Property Name="DistPart[0].SoftDep[5].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[0].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[6].productName" Type="Str">NI LabVIEW Web Server 2020</Property>
				<Property Name="DistPart[0].SoftDep[6].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[0].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[7].productName" Type="Str">NI mDNS Responder 19.0</Property>
				<Property Name="DistPart[0].SoftDep[7].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[0].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[8].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[0].SoftDep[8].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[0].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[9].productName" Type="Str">Math Kernel Libraries 2020</Property>
				<Property Name="DistPart[0].SoftDep[9].upgradeCode" Type="Str">{9872BBBA-FB96-42A4-80A2-9605AC5CBCF1}</Property>
				<Property Name="DistPart[0].SoftDepCount" Type="Int">12</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{D84FC73F-D1E0-4C05-A30C-DB882CD1ABD8}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[1].productID" Type="Str">{B53A92BE-1BFE-43BA-9EA7-EA42377EA4AE}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI GigE Vision 20.6.0</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{05544A20-4A6F-4EF3-B202-CFE6C5E6041C}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[2].productID" Type="Str">{944CC86F-BDFB-4850-878C-370B9A7FF12C}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI-VISA Runtime 20.0</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[3].productID" Type="Str">{06CE04CA-A6D6-4353-B169-328BE6B7046C}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI-IMAQdx Runtime 20.6</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{3D104AB3-CE10-43C0-B647-07600754072C}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[4].productID" Type="Str">{84EDD624-82A5-4A41-8D30-CA43D7560540}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI-DAQmx Runtime 20.1</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{923C9CD5-A0D8-4147-9A8D-998780E30763}</Property>
				<Property Name="DistPart[5].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[5].productID" Type="Str">{13B38F6E-AF24-442E-8326-9B8B085C4992}</Property>
				<Property Name="DistPart[5].productName" Type="Str">NI Vision Runtime 2020 SP1</Property>
				<Property Name="DistPart[5].upgradeCode" Type="Str">{63DF74E5-A5C9-11D4-814E-005004D6CDD6}</Property>
				<Property Name="DistPart[6].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[6].productID" Type="Str">{E7491C92-6FEB-4CD1-8F74-1C6909D1FBAD}</Property>
				<Property Name="DistPart[6].productName" Type="Str">NI Variable Engine 2019</Property>
				<Property Name="DistPart[6].upgradeCode" Type="Str">{EB7A3C81-1C0F-4495-8CE5-0A427E4E6285}</Property>
				<Property Name="DistPartCount" Type="Int">7</Property>
				<Property Name="INST_author" Type="Str">FIT</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../builds/AVI1.0/LB Installer</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">LB Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{775A20AE-CFB2-4C64-9D05-7D288EB3285B}</Property>
				<Property Name="INST_installerName" Type="Str">install.exe</Property>
				<Property Name="INST_productName" Type="Str">AVI1.0</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.2</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">20008014</Property>
				<Property Name="MSI_arpCompany" Type="Str">FIT</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.fit.com/</Property>
				<Property Name="MSI_distID" Type="Str">{797490F0-5D01-4939-98EB-7D7C5D939A82}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{51D005E3-A18B-4E14-B784-FE8F78DADE34}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{775A20AE-CFB2-4C64-9D05-7D288EB3285B}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{775A20AE-CFB2-4C64-9D05-7D288EB3285B}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">AVI1.0.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">AVI1.0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">AVI1.0</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{F38A68ED-A616-49ED-A8FF-273C3D9455DD}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">AVI1.0</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/AVI1.0</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="Source[1].dest" Type="Str">{775A20AE-CFB2-4C64-9D05-7D288EB3285B}</Property>
				<Property Name="Source[1].name" Type="Str">ModbusIO_AVI.lvlib</Property>
				<Property Name="Source[1].tag" Type="Ref">/My Computer/ModbusIO_AVI.lvlib</Property>
				<Property Name="Source[1].type" Type="Str">File</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
			</Item>
		</Item>
	</Item>
</Project>
