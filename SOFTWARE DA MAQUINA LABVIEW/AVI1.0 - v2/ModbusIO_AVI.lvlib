﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2020_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2020\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2020_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2020\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="000521" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000521</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000521</Property>
	</Item>
	<Item Name="000522" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000522</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000522</Property>
	</Item>
	<Item Name="000523" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000523</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000523</Property>
	</Item>
	<Item Name="000527" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000527</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000527</Property>
	</Item>
	<Item Name="100597" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100597</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100597</Property>
	</Item>
	<Item Name="100598" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100598</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100598</Property>
	</Item>
	<Item Name="100599" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100599</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100599</Property>
	</Item>
	<Item Name="100610" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100610</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100610</Property>
	</Item>
	<Item Name="100611" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100611</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100611</Property>
	</Item>
	<Item Name="100612" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100612</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100612</Property>
	</Item>
	<Item Name="100613" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100613</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100613</Property>
	</Item>
	<Item Name="100614" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100614</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100614</Property>
	</Item>
	<Item Name="100623" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100623</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100623</Property>
	</Item>
	<Item Name="100629" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100629</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100629</Property>
	</Item>
	<Item Name="100630" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100630</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100630</Property>
	</Item>
	<Item Name="100631" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100631</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100631</Property>
	</Item>
	<Item Name="Amarelo_ent" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000514</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#!!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000514</Property>
	</Item>
	<Item Name="Amarelo_sai" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000516</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#!!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000516</Property>
	</Item>
	<Item Name="Conveyor" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000526</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000526</Property>
	</Item>
	<Item Name="Emerg1" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100624</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#!!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100624</Property>
	</Item>
	<Item Name="Emerg2" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100625</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#!!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100625</Property>
	</Item>
	<Item Name="Emerg3" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100626</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#!!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100626</Property>
	</Item>
	<Item Name="Emerg4" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100627</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#!!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100627</Property>
	</Item>
	<Item Name="Flipper" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000518</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000518</Property>
	</Item>
	<Item Name="Illumination" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000517</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000517</Property>
	</Item>
	<Item Name="Modbus1" Type="IO Server">
		<Property Name="atrb" Type="Str">C!-!!!Q1!!!8!!!!"B!!!!=!!!""!'1!:!"S!'5!=Q"T!!-!"1!!!!!!!!$Q0Q91!!!)!!!!1A"B!(5!:!"3!'%!&gt;!"F!!-!"A!!!!!!!-$#1!91!!!(!!!!2!"B!(1!91"#!'E!&gt;!!$!!5!!!!!!!!!)%!'%!!!#1!!!%E!5!""!'1!:!"S!'5!=Q"T!!91!!!0!!!!-1!W!$E!,A!S!$5!.!!O!$%!.A!R!#Y!-1!Q!$%!"B!!!!Y!!!"*!&amp;!!11"E!'1!=A"F!(-!=Q""!'Q!;1"B!(-!"B!!!!]!!!!R!$9!/1!O!$)!.1!U!#Y!-1!W!$%!,A!R!$!!-1!'%!!!#!!!!%Q!5Q"8!%9!;1"S!(-!&gt;!!#!!!!!!91!!!(!!!!41"B!(A!1Q"P!'E!&lt;!!$!!5!!!!!!!$AHE!'%!!!#Q!!!%U!91"Y!%1!;1"T!'-!=A"F!(1!:1!$!!5!!!!!!!"!HU!'%!!!#A!!!%U!91"Y!%A!&lt;Q"M!'1!;1"O!'=!!Q!&amp;!!!!!!!!Q&amp;Z!"B!!!!A!!!".!'%!?!"*!'Y!=!"V!(1!!Q!&amp;!!!!!!!!1&amp;^!"B!!!!5!!!".!']!:!"F!'Q!"B!!!!]!!!".!']!:!"C!(5!=Q!A!%5!&gt;!"I!'5!=A"O!'5!&gt;!!'%!!!"!!!!%Y!91"N!'5!"B!!!!=!!!".!']!:!"C!(5!=Q!R!!91!!!'!!!!5!"B!()!;1"U!(E!"B!!!!1!!!"O!']!&lt;A"F!!91!!!(!!!!5!"I!']!&lt;A"F!%Y!&lt;Q!'%!!!!!!!!!91!!!)!!!!5!"P!'Q!&lt;!"3!'%!&gt;!"F!!-!!!"\&amp;+Z(Y8K%0Q91!!!)!!!!5!"S!'E!&lt;Q"S!'E!&gt;!"Z!!-!"1!!!!!!!!!A1!91!!!(!!!!5A"F!(1!=A"J!'5!=Q!$!!5!!!!!!!!!%%!'%!!!#A!!!&amp;-!:1"S!'E!91"M!&amp;!!&lt;Q"S!(1!"B!!!!1!!!"$!%]!41!R!!91!!!&amp;!!!!5Q"L!'E!=!"T!!-!"1!!!!!!!!$Q0Q91!!!(!!!!5Q"U!']!=!"#!'E!&gt;!!'%!!!!1!!!$%!"B!!!!=!!!"5!'E!&lt;1"F!']!&gt;1"U!!-!"A!!!!!!!!"*1!91!!!*!!!!6!"S!'%!&lt;A"T!%U!&lt;Q"E!'5!"B!!!!-!!!"3!&amp;1!61!'%!!!"Q!!!&amp;5!=Q"F!%9!1Q!R!$9!!A!!!!!</Property>
		<Property Name="className" Type="Str">Modbus</Property>
	</Item>
	<Item Name="R_Emerg" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100596</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100596</Property>
	</Item>
	<Item Name="R_Error" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100594</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100594</Property>
	</Item>
	<Item Name="R_Ready" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100592</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#!!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100592</Property>
	</Item>
	<Item Name="R_Run" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100593</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#!!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100593</Property>
	</Item>
	<Item Name="R_Warning" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100595</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100595</Property>
	</Item>
	<Item Name="Reset_Sec" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000524</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000524</Property>
	</Item>
	<Item Name="Rst_Rob" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000600</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000600</Property>
	</Item>
	<Item Name="S1_Flipper" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100608</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100608</Property>
	</Item>
	<Item Name="S2_Flipper" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100609</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100609</Property>
	</Item>
	<Item Name="Sensor1_Conveyor" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100628</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100628</Property>
	</Item>
	<Item Name="Sensor1_Vacg" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100620</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100620</Property>
	</Item>
	<Item Name="Sensor2_Vacg" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100621</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100621</Property>
	</Item>
	<Item Name="Sensor_Berco" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100615</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#!!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100615</Property>
	</Item>
	<Item Name="Sensor_Ent" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100617</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!#!!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100617</Property>
	</Item>
	<Item Name="Sensor_Out" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100616</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100616</Property>
	</Item>
	<Item Name="Sensor_Press" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100619</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100619</Property>
	</Item>
	<Item Name="Sensor_Reject" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100618</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100618</Property>
	</Item>
	<Item Name="Sensor_Vacf" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\100622</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">100622</Property>
	</Item>
	<Item Name="Signal_Sec" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000525</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000525</Property>
	</Item>
	<Item Name="Stop_Rob" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000601</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000601</Property>
	</Item>
	<Item Name="Vacc_gant" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000519</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000519</Property>
	</Item>
	<Item Name="Vaccu_Flip" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000520</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000520</Property>
	</Item>
	<Item Name="Verde_ent" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000512</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000512</Property>
	</Item>
	<Item Name="Verde_sai" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000515</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000515</Property>
	</Item>
	<Item Name="Vermelho" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\000513</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/AVI1.0.lvproj/My Computer/ModbusIO_AVI.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!#!!A!!!!!!"!!1!)1!"!!!"!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusIO_AVI.lvlib\Modbus1\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000513</Property>
	</Item>
</Library>
