-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: mnsnt066    Database: cqa_v2
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `sa78d21898`
--

DROP TABLE IF EXISTS `sa78d21898`;
/*!50001 DROP VIEW IF EXISTS `sa78d21898`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `sa78d21898` AS SELECT 
 1 AS `uid`,
 1 AS `track_id`,
 1 AS `result`,
 1 AS `model`,
 1 AS `region_id`,
 1 AS `filename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `sa78d21897`
--

DROP TABLE IF EXISTS `sa78d21897`;
/*!50001 DROP VIEW IF EXISTS `sa78d21897`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `sa78d21897` AS SELECT 
 1 AS `uid`,
 1 AS `track_id`,
 1 AS `result`,
 1 AS `model`,
 1 AS `region_id`,
 1 AS `filename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ultimos`
--

DROP TABLE IF EXISTS `ultimos`;
/*!50001 DROP VIEW IF EXISTS `ultimos`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `ultimos` AS SELECT 
 1 AS `uid`,
 1 AS `track_id`,
 1 AS `result`,
 1 AS `model`,
 1 AS `region_id`,
 1 AS `region_result`,
 1 AS `filename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `sa78d21899`
--

DROP TABLE IF EXISTS `sa78d21899`;
/*!50001 DROP VIEW IF EXISTS `sa78d21899`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `sa78d21899` AS SELECT 
 1 AS `uid`,
 1 AS `track_id`,
 1 AS `result`,
 1 AS `model`,
 1 AS `region_id`,
 1 AS `filename`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `sa78d21862`
--

DROP TABLE IF EXISTS `sa78d21862`;
/*!50001 DROP VIEW IF EXISTS `sa78d21862`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `sa78d21862` AS SELECT 
 1 AS `uid`,
 1 AS `track_id`,
 1 AS `result`,
 1 AS `model`,
 1 AS `region_id`,
 1 AS `filename`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `sa78d21898`
--

/*!50001 DROP VIEW IF EXISTS `sa78d21898`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`fit_inventario`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `sa78d21898` AS select `a`.`uid` AS `uid`,`a`.`track_id` AS `track_id`,`a`.`result` AS `result`,`a`.`model` AS `model`,`b`.`region_id` AS `region_id`,`b`.`filename` AS `filename` from (`registered_products` `a` join (select distinct `registered_regions`.`uid` AS `uid`,`registered_regions`.`region_id` AS `region_id`,`registered_regions`.`filename` AS `filename`,`inspected_images`.`result` AS `result` from (`registered_regions` join `inspected_images` on((`registered_regions`.`region_id` = `inspected_images`.`id_region`)))) `b` on((`a`.`uid` = `b`.`uid`))) where ((`a`.`result` = 'OK') and (`a`.`model` = 'SA78D21898')) order by `a`.`uid` desc limit 20000 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `sa78d21897`
--

/*!50001 DROP VIEW IF EXISTS `sa78d21897`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`fit_inventario`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `sa78d21897` AS select `a`.`uid` AS `uid`,`a`.`track_id` AS `track_id`,`a`.`result` AS `result`,`a`.`model` AS `model`,`b`.`region_id` AS `region_id`,`b`.`filename` AS `filename` from (`registered_products` `a` join (select distinct `registered_regions`.`uid` AS `uid`,`registered_regions`.`region_id` AS `region_id`,`registered_regions`.`filename` AS `filename`,`inspected_images`.`result` AS `result` from (`registered_regions` join `inspected_images` on((`registered_regions`.`region_id` = `inspected_images`.`id_region`)))) `b` on((`a`.`uid` = `b`.`uid`))) where ((`a`.`result` = 'OK') and (`a`.`model` = 'SA78D21897')) order by `a`.`uid` desc limit 20000 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ultimos`
--

/*!50001 DROP VIEW IF EXISTS `ultimos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`fit_inventario`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `ultimos` AS select `a`.`uid` AS `uid`,`a`.`track_id` AS `track_id`,`a`.`result` AS `result`,`a`.`model` AS `model`,`b`.`region_id` AS `region_id`,`b`.`result` AS `region_result`,`b`.`filename` AS `filename` from (`registered_products` `a` join (select distinct `registered_regions`.`uid` AS `uid`,`registered_regions`.`region_id` AS `region_id`,`registered_regions`.`filename` AS `filename`,`inspected_images`.`result` AS `result` from (`registered_regions` join `inspected_images` on((`registered_regions`.`region_id` = `inspected_images`.`id_region`)))) `b` on((`a`.`uid` = `b`.`uid`))) where ((`a`.`result` = 'OK') and (`a`.`model` = 'SA78D21898')) order by `a`.`uid` desc limit 20000 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `sa78d21899`
--

/*!50001 DROP VIEW IF EXISTS `sa78d21899`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`fit_inventario`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `sa78d21899` AS select `a`.`uid` AS `uid`,`a`.`track_id` AS `track_id`,`a`.`result` AS `result`,`a`.`model` AS `model`,`b`.`region_id` AS `region_id`,`b`.`filename` AS `filename` from (`registered_products` `a` join (select distinct `registered_regions`.`uid` AS `uid`,`registered_regions`.`region_id` AS `region_id`,`registered_regions`.`filename` AS `filename`,`inspected_images`.`result` AS `result` from (`registered_regions` join `inspected_images` on((`registered_regions`.`region_id` = `inspected_images`.`id_region`)))) `b` on((`a`.`uid` = `b`.`uid`))) where ((`a`.`result` = 'OK') and (`a`.`model` = 'SA78D21899')) order by `a`.`uid` desc limit 20000 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `sa78d21862`
--

/*!50001 DROP VIEW IF EXISTS `sa78d21862`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`fit_inventario`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `sa78d21862` AS select `a`.`uid` AS `uid`,`a`.`track_id` AS `track_id`,`a`.`result` AS `result`,`a`.`model` AS `model`,`b`.`region_id` AS `region_id`,`b`.`filename` AS `filename` from (`registered_products` `a` join (select distinct `registered_regions`.`uid` AS `uid`,`registered_regions`.`region_id` AS `region_id`,`registered_regions`.`filename` AS `filename`,`inspected_images`.`result` AS `result` from (`registered_regions` join `inspected_images` on((`registered_regions`.`region_id` = `inspected_images`.`id_region`)))) `b` on((`a`.`uid` = `b`.`uid`))) where ((`a`.`result` = 'OK') and (`a`.`model` = 'SA78D21862')) order by `a`.`uid` desc limit 20000 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-09 14:51:43
