<?php

$link = new MySQLi('mnsnt066', 'fit_inventario', 'Flex@2k20', 'cqa');
if($link->connect_error){
   echo "Desconectado! Erro: " . $link->connect_error;
}


$consulta="
SELECT 
   LABEL, COUNT(LABEL) AS quantidade
FROM
    cqa_v2.manual_classification
WHERE
    DATA BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() + INTERVAL 1 DAY
GROUP BY LABEL order by quantidade desc;
";


$con = $link->query($consulta) or die($link->error);

$legenda=[];
$data=[];

while($dado = $con->fetch_array()) {
  
    array_push($data,array($dado[0],intval($dado[1])));
    array_push($legenda,$dado[0].":".$dado[1]);
    //$legenda[]=$dado[0];
}

//print_r($data);
//print_r($legenda);

#incluindo a classe. verifique se diretorio e versao sao iguais, altere se precisar
include('phplot/phplot.php');

$plot = new PHPlot(700,400);
$plot->SetImageBorderType('plain');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetTitle("Labels : Last 30 Days");
$plot->SetPlotType('pie');
$colors = array('red', 'green', 'blue', 'yellow', 'gray','cyan','navy','wheat','violet','brown','gold','SkyBlue','peru');
$plot->SetDataColors($colors);
$plot->SetLegend($legenda);
$plot->SetShading(0);
$plot->SetLabelScalePosition(0.2);
$plot->DrawGraph();


?>