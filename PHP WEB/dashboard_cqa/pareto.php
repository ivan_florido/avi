<?php

$link = new MySQLi('127.0.0.1', 'fit_inventario', 'Flex@2k20', 'cqa_v2');
if($link->connect_error){
   echo "Desconectado! Erro: " . $link->connect_error;
}


#$consulta="SELECT * FROM manual_classification"; 

$consulta="select day(manual_classification.data) as 'Dia', count(*) as 'Quantidade' from manual_classification where data BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()+ INTERVAL 1 DAY group by Dia;";

$con = $link->query($consulta) or die($link->error);

$x=[];

while($dado = $con->fetch_array()) {
    #echo $dado['ID']; 
    #echo $dado['UUID']; 
    #echo $dado['MODELO']; 
    #echo $dado['TRACK_ID']; 
    #echo $dado['REGION']; 
    #echo $dado['LABEL']; 
    #echo $dado['C_LINHA']; 
    $var[]=array($dado['Dia'],$dado['Quantidade']);
   
    #echo "<br>";
 }
 


#incluindo a classe. verifique se diretorio e versao sao iguais, altere se precisar
include('phplot/phplot.php');

$plot = new PHPlot(1000, 400);
$plot->SetImageBorderType('plain');

$plot->SetPlotType('bars');
$plot->SetDataType('text-data');
$plot->SetDataValues($var);

# Main plot title:
$plot->SetTitle('Coleta por Dia CQA');
$plot->SetXTitle('Dia');
$plot->SetYTitle('Quantidade');
# Make a legend for the 3 data sets plotted:
#$plot->SetLegend(array('Engineering', 'Manufacturing', 'Administration'));
$plot->SetYDataLabelPos('plotin');
# Turn off X tick labels and ticks because they don't apply here:
$plot->SetXTickLabelPos('none');
$plot->SetXTickPos('none');

$plot->DrawGraph();
?>