<?php
require("conecta.php");

$numero_imagens = 10;
$machine_id="CQA_v3";
date_default_timezone_set('America/Manaus'); // padrao horario manaus
$today = date("Y-m-d H:i:s"); // data atual

if(!empty($_GET['descricao']))
{

    //para remoever acentos
    
    $descricao_old = $_GET['descricao'];

    $caracteres_sem_acento = array(
    'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj',''=>'Z', ''=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
    'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
    'Ï'=>'I', 'Ñ'=>'N', 'Ń'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
    'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
    'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
    'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ń'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
    'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',"%a"=>'_',
    'ă'=>'a', 'î'=>'i', 'â'=>'a', 'ș'=>'s', 'ț'=>'t', 'Ă'=>'A', 'Î'=>'I', 'Â'=>'A', 'Ș'=>'S', 'Ț'=>'T',' '=>'_',
    );

    $descricao = strtr($descricao_old, $caracteres_sem_acento);

    //para remover acentos fim 


    if(!empty($_GET['codigo']) &&  !empty($_GET['track_id'])){

        $track_id="NA";
        $codigo=0;

    }

    if(empty($_GET['codigo'])){
        $codigo=0;
    }else{
        $codigo=$_GET['codigo'];
    }   
    
    if(empty($_GET['track_id'])){
        $track_id="NA";
    }else{
        $track_id=$_GET['track_id'];
    } 

    //$track_id=$_GET['track_id'];    

    $consulta= "INSERT INTO `registered_erros`
    (`id_erro`,
    `descricao`,
    `codigo`,
    `track_id`)
    VALUES
    (NULL,
    '$descricao',
    '$codigo',
    '$track_id');";

    $con = $link->query($consulta) or die($link->error);

    echo "1";
 
}else{

?>
<html>
<head>
    <title>AVI - PROJECT - LOG DE ERROS</title>
<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
-->

<?php
include('style.php');
?>       
        
  
		
		<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
		<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" charset="utf-8">
			

            $.extend( true, $.fn.dataTable.defaults, {
                "searching": true,
                "ordering": true
            } );
            
            
            $(document).ready(function() {
                $('#example').DataTable( {
                "order": [[ 0, "desc" ]]
        
            } );
} );

</script>
</head>
<body>
<body>
<a href="http://mnsnt066/cqa/" id="home"><b> -Home<--</b></a><br>

<table id="example" class="display" style="width:100%">
    <thead>
        <tr>
            <th>ID ERROR</th>
            <th>DESCRIÇÃO</th>
            <th>CODIGO</th>
            <th>Track_id</th>
            <th>DATA</th>         
            
        </tr>
    </thead>

    <tbody>


<?php



    echo "<h1><b>LISTA DE ERROS ESTAÇÃO AVI:</h1><br><br>";

    $consulta = "SELECT * FROM `registered_erros` ORDER BY `id_erro` DESC LIMIT 1000"; 
    $con = $link->query($consulta) or die($link->error);   

    while($dado = $con->fetch_array()) {
        echo "<tr>";
        echo "<td>".$dado['id_erro']."</td>";
        echo "<td>".$dado['descricao']."</td>";
        echo "<td>".$dado['codigo']."</td>";
        echo "<td>".$dado['track_id']."</td>";
        echo "<td>".$dado['data']."</td>";
        echo "</tr>";   
    } 

    echo "</table></body>";

    
}

?>