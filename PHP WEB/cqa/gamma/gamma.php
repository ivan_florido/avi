<?php

header("Content-Type: image/bmp");

$gamma=$_GET['gamma'];
$img=$_GET['filename'];

$local_file="D:/automation/cqa/images/raw/".$img;

$gamma_c=$gamma+1.0;

// Create image instance
$im = imagecreatefrombmp($local_file);

// Correct gamma, out = 1.537
imagegammacorrect($im, 1.0, $gamma_c);

// Save and free image
//imagebmp($im, './119610_ZF523R7LB5_2022-3-25_4_1.5.bmp');
imagebmp($im);


imagedestroy($im);

?>