<?php
/****************************************************************************************************************/
//config start

require("conecta.php");             // mysql conection open

$img=$_GET['file'];                 // obtem o nome do arquivo enviado via get
$local_file="D:/automation/cqa/images/raw/".$img;
//$server = "mnsnt066";               //nome do servidor    
//$FTP_HOST = "mnsnt066";             //endereço do servidor de ftp
//$FTP_USER = "fit_ftp";              //user servidor de ftp
//$FTP_PASS = "Flex@2k20";            //senha do servidor de FTP
//$pasta=     "D:/webserver/dashboard_cqa/tmp/";  // pasta temporaria
//$local_file = $pasta.$img;

//$server_file = "/raw/$img";
/*
/***************************************************************************************************************/

/***************************************clean temp files start*************************************************
    if(is_dir($pasta)){

        $diretorio = dir($pasta);

        //limpa o diretorio tmp
        while($arquivo = $diretorio->read()){
            if(($arquivo != '.') && ($arquivo != '..')){
                unlink($pasta.$arquivo);
                //echo 'Arquivo '.$arquivo.' foi apagado com sucesso. <br />';
            }
        }
        $diretorio->close();
    }else
    {
        echo 'A pasta não existe.';
    }


    //abre o FTP e pega a imagem
    $cHandle = ftp_connect($FTP_HOST) or die("O Servidor não pode se conectar ao FTP");
    $login_result = ftp_login($cHandle, $FTP_USER, $FTP_PASS) or die("O Servidor não pode logar-se no FTP!");
    ftp_get($cHandle, $local_file, $server_file, FTP_BINARY);
    ftp_close($cHandle);


  */
/******************************************FTP open and download file end***************************************/

    if(!empty($_GET["file"])){ // check if filename exist in get
      
        $ext_nome=explode(".",$_GET["file"]);
        $file_ext=$ext_nome[1];
        
       
        if($file_ext=="bmp"){
            header("Content-Type: image/bmp");  // define header
            $im1 = imagecreatefrombmp($local_file);  // Load the stamp and the photo
        }else{
            header("Content-Type: image/png");  // define header
            $im1 = imagecreatefrompng($local_file);  // Load the stamp and the photo
        }

        
        $im = imagecreatetruecolor(imagesx($im1), imagesy($im1)); // convert a truecolor
        imagecopy($im, $im1, 0, 0, 0, 0, imagesx($im1), imagesy($im1)); //copy img
        
        /*define colors start*/
        $red = imagecolorallocate($im, 255, 0, 0);   
        $green = imagecolorallocate($im, 0, 255, 0);
        $blue = imagecolorallocate($im, 0, 0, 255);
        $yellow = imagecolorallocate($im, 255, 255, 0);
        /*define colors end*/


        //check the region id in bd
        $id=0;
        $consulta = "SELECT * FROM registered_regions where filename='$_GET[file]'"; 
        $con = $link->query($consulta) or die($link->error);
    
        while($dado = $con->fetch_array()) {
            $id = $dado['region_id'];
        } 
        //check region id end

        
        
        if($id){  // check if region id has found
            /********************************************************************************************/
            //cognex sql consult and printer start 
            $consulta = "SELECT * FROM inspected_images_cgx_teste where id_region='$id' "; 
            $con = $link->query($consulta) or die($link->error);
            $resultado="";
            $i=0;
            $incremento=75;

            while($dado = $con->fetch_array()) {
                $valor=$incremento*$i;

                imagettftext($im,20,0,5,25,$red,"arial.ttf","IA - Cognex");
               
                imagearc($im, $dado['cx'],$dado['cy'],100,100,100,100,$red); 
                imagettftext($im,20,0,$dado['cx']+($dado['width']/2),$dado['cy']-($dado['height']/2),$red,"arial.ttf",$i);               
                imagerectangle($im, $dado['x0'],$dado['y0'],$dado['x1'],$dado['y1'], $red);             
                imagettftext($im,20,0,5,50,$red,"arial.ttf","file:".$_GET['file']);
                imagettftext($im,20,0,5,75,$red,"arial.ttf","Data:".$dado['date']);
                imagettftext($im,20,0,5,100,$red,"arial.ttf","uid:".$dado['uid']);
                imagettftext($im,20,0,5,125,$red,"arial.ttf","region id:".$dado['id_region']);

                imagettftext($im,20,0,5,150+$valor,$red,"arial.ttf",$i."-->score:".$dado['classification_score']);
                imagettftext($im,20,0,5,175+$valor,$red,"arial.ttf",$i."-->Area:".$dado['area']."; CX:".$dado['cx']."; CY:".$dado['cy']."; W:".$dado['width']."; H:".$dado['height']);
                imagettftext($im,20,0,5,200+$valor,$red,"arial.ttf",$i."-->Reprovado:".$dado['result']);
     
                
                $i=$i+1;
                
            }
            //cognex sql consult and printer end
            /***********************************************************************************************/

            /**********************************************************************************************/  
            //manual classification consult start  
            $consulta = "SELECT * FROM manual_classification where id_region='$id' "; 
            $con = $link->query($consulta) or die($link->error);
            $resultado="";
            $i=0;


            $incremento=25;
            while($dado = $con->fetch_array()) {
                $new_pos=200+$valor+$incremento*$i;

                imagettftext($im,20,0,5,$valor+225,$green,"arial.ttf","Manual - Operador");              
                $c_x= $dado['X_INICIO'] + (($dado['X_FIM']-$dado['X_INICIO'])/2);
                $c_y= $dado['Y_INICIO']+ (($dado['Y_FIM']-$dado['Y_INICIO'])/2);
                imagettftext($im,20,0,5,50+$new_pos,$green,"arial.ttf",$i."-->Tipo: ".$dado['LABEL']);
                imagettftext($im,20,0,$c_x+(($dado['X_FIM']-$dado['X_INICIO'])/2),$c_y-(($dado['Y_FIM']-$dado['Y_INICIO'])/2),$green,"arial.ttf",$i); 
                imagerectangle($im, $dado['X_INICIO'],$dado['Y_INICIO'],$dado['X_FIM'],$dado['Y_FIM'], $green);
                imageellipse($im,$c_x,$c_y,($dado['X_FIM']-$dado['X_INICIO']),($dado['Y_FIM']-$dado['Y_INICIO']),$green); 

                $i=$i+1;

                //$new_pos=25+$valor+$incremento*$i;
            }

            //manual classification end
            /************************************************************************************************/
            
            //cognex sql consult and printer start 
            $consulta = "SELECT * FROM inspected_images_ngene where id_region='$id' "; 
            $con = $link->query($consulta) or die($link->error);
            $resultado="";
            $i=0;
            $incremento=75;

            while($dado = $con->fetch_array()) {

                $new_pos=200+$valor+$incremento*$i;
                imagettftext($im,20,0,5,$valor+225,$yellow,"arial.ttf","IA-ngene");
                imagearc($im, $dado['cx'],$dado['cy'],100,100,100,100,$yellow); 
                imagerectangle($im, $dado['x0'],$dado['y0'],$dado['x1'],$dado['y1'], $yellow);          
           
                $i=$i+1;
    
            }
//cognex sql consult and printer end





            /************************************************************************************************/
            //opencadd bd consult and print start

            $consulta = "SELECT * FROM inspected_images_opc where id_region='$id' "; 

            $con = $link->query($consulta) or die($link->error);
            $resultado="";
            $i=0;

            while($dado = $con->fetch_array()) {
        
                imagettftext($im,45,0,2010,100,$blue,"arial.ttf","IA - OpenCadd");
                imagearc($im, $dado['cx'],$dado['cy'],100,100,100,100,$blue);
            }
            //copencadd consult end
            /***********************************************************************************************/

            /*****
            
            //check and printer the cognex test bd
            $consulta = "SELECT * FROM inspected_images_cgx_teste where id_region='$id' ";
            $con = $link->query($consulta) or die($link->error);
            $resultado="";
            $i=0;

            while($dado = $con->fetch_array()) {
                imagettftext($im,45,0,1510,100,$yellow,"arial.ttf","IA - Cognex Teste");
                imagearc($im, $dado['cx'],$dado['cy'],100,100,100,100,$yellow);
            }   
            **/


        }
        else{
                echo "-1";

            }
    }
            
        
         
    if($file_ext=="bmp"){
        imagepng($im);
    }else{
        imagebmp($im); 
    }
?>