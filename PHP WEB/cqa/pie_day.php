<?php

$link = new MySQLi('127.0.0.1', 'fit_inventario', 'Flex@2k20', 'cqa_v2');
if($link->connect_error){
   echo "Desconectado! Erro: " . $link->connect_error;
}


$consulta="SELECT day(date) AS dia, result,COUNT(result) AS unico
FROM registered_products where date(date)= curdate()
GROUP BY result
ORDER BY dia ASC;";

$con = $link->query($consulta) or die($link->error);

$x=[];
$nok=0;
$ok=0;

while($dado = $con->fetch_array()) {
    
    if($dado['result'] == "NOK"){
        $nok=$dado['unico'];
        
    }
    if($dado['result'] =="OK"){
        $ok=$dado['unico'];
        
    }    
 }
 



#incluindo a classe. verifique se diretorio e versao sao iguais, altere se precisar
include('phplot/phplot.php');

# PHPlot Example:  Flat Pie with options


$data = array(
  array('REPROVADO', $nok),
  array('APROVADO', $ok) 
);

$plot = new PHPlot(300,300);
$plot->SetTitle('Resultado por dia');
$plot->SetImageBorderType('plain');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetPlotType('pie');

$colors = array('red', 'green');
$plot->SetDataColors($colors);
$plot->SetLegend(array('REPROVADO','APROVADO'));
$plot->SetShading(0);
$plot->SetLabelScalePosition(0.2);

$plot->DrawGraph();

?>