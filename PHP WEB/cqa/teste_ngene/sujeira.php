<?php

//modulo para teste do servidor da ngene
//28/04/2022

$servidor_ngene_2="mnsm0725"; // servidor onde esta a IA da ngene
$servidor_ngene_2_port=17;    //porta a ser utilizada
$msg="";                      //msg inicial 


if(!empty($_GET['filename']) and !empty($_GET['cx']) and !empty($_GET['cx']) ){ // verifica se oos parametros post foram enviados

    $file_name=$_GET['filename'];
    $cx=$_GET['cx'];
    $cy=$_GET['cy'];
    $msg="$file_name,$cx,$cy";

}else{
    echo "Falta parametro (exemplo: ?filename=126954_ZF523S9287_2022-4-28_6.bmp&cx=681&cy=512)<br>";
    //$file_name="126954_ZF523S9287_2022-4-28_6.bmp";
    //$cx="681";
    //$cy="512";
    //$msg="$file_name,$cx,$cy";
}

$socket_ngene_1 = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

if(!empty($msg)){

    if(@socket_connect($socket_ngene_1, $servidor_ngene_2, $servidor_ngene_2_port)){
        
        $time_start = microtime(true); //start the processing time count        
        socket_write($socket_ngene_1, $msg); 
        $ngene_2_result = trim(socket_read($socket_ngene_1, 1024));
        socket_close($socket_ngene_1);        
        $time_end = microtime(true);
        $time1 = $time_end - $time_start;
        $time=$time1*1000;
        echo "$msg,$ngene_2_result,$time";

    }else{
        echo "<b> ERROR-1 (Servidor esta Offline)</b>"; //-1 erro se o servidor esta offline
    }

}else{
    echo "<b>ERROR: -2 (Falta de parametros)</b>"; // -2 erro quando nao se tem msg
}
