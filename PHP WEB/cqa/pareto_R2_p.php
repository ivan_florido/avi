<?php

$link = new MySQLi('mnsnt066', 'fit_inventario', 'Flex@2k20', 'cqa_v2');
if($link->connect_error){
   echo "Desconectado! Erro: " . $link->connect_error;
}


#$consulta="SELECT * FROM manual_classification"; 
/*
$consulta="select
day(registered_products.date) as 'Dia',
count(*) as 'Quantidade'
from
registered_products
where
registered_products.date BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()
group by 
Dia;";
*/


/*
$consulta_ttl="select day(inspected_images.date) as 'Dia', count(*) as 'Quantidade' from inspected_images  where  region='2' and inspected_images.date BETWEEN CURDATE() - INTERVAL 20 DAY AND CURDATE()+ INTERVAL 1 DAY group by 
Dia;"; 
*/


$consulta_ttl="select  day(inspected_images.date) as 'Dia', count(distinct id_region) as 'unico' from inspected_images  where region=2 and inspected_images.date BETWEEN CURDATE() - INTERVAL 22 DAY AND CURDATE()+ INTERVAL 1 DAY group by 
Dia;";

/*
$consulta_cognex_def="select day(inspected_images.date) as 'Dia', count(*) as 'Quantidade' from inspected_images  where region=2 and  result=1 and inspected_images.date BETWEEN CURDATE() - INTERVAL 20 DAY AND CURDATE()+ INTERVAL 1 DAY group by 
Dia;"; 
*/

$consulta_cognex_def="select  day(inspected_images.date) as 'Dia', count(distinct id_region) as 'unico' from inspected_images  where region=2 and result=1 and inspected_images.date BETWEEN CURDATE() - INTERVAL 22 DAY AND CURDATE()+ INTERVAL 1 DAY group by 
Dia;";

/*
$consulta_ngene_def="select day(inspected_images_ngene.date) as 'Dia', count(*) as 'Quantidade' from inspected_images_ngene  where  result=1 and inspected_images_ngene.date BETWEEN CURDATE() - INTERVAL 20 DAY AND CURDATE()+ INTERVAL 1 DAY group by 
Dia;";
*/
$consulta_ngene_def="select  day(inspected_images_ngene.date) as 'Dia', count(distinct id_region) as 'unico' from inspected_images_ngene  where region=2 and result=1 and inspected_images_ngene.date BETWEEN CURDATE() - INTERVAL 22 DAY AND CURDATE()+ INTERVAL 1 DAY group by 
Dia;";

/*
$consulta_opencadd_def="select day(inspected_images_validations.date) as 'Dia', count(*) as 'Quantidade' from inspected_images_validations  where  result=1 and inspected_images_validations.date BETWEEN CURDATE() - INTERVAL 20 DAY AND CURDATE()+ INTERVAL 1 DAY group by 
Dia;";
*/

$consulta_opencadd_def="select  day(inspected_images_validations.date) as 'Dia', count(distinct id_region) as 'unico' from inspected_images_validations  where region=2 and result=1 and inspected_images_validations.date BETWEEN CURDATE() - INTERVAL 22 DAY AND CURDATE()+ INTERVAL 1 DAY group by 
Dia;";



//$defeitos_dia[0]=array(0,0,0,0,0);
$i=1;
//echo $consulta;
while($i<32){
    //$defeitos_cognex[$i]=0;
    //$defeitos_ngene[$i]=0;
    //$defeitos_opencadd[$i]=0;
    //$defeitos_dia[$i]=array(0,0,0,0,0);
    //$defeitos_total[$i]=0;
    $i++;    
}
$defeitos_total=[];
$con = $link->query($consulta_ttl) or die($link->error);
while($dado = $con->fetch_array()) { 
    
    $dia=$dado['Dia'];
    $quantidade=$dado['unico'];    
    $defeitos_total+=[$dia=>$quantidade]; 
    
}
//print_r($defeitos_total);
$defeitos_cognex=[];
$con = $link->query($consulta_cognex_def) or die($link->error);
while($dado = $con->fetch_array()) { 
    $dia=$dado['Dia'];
    $quantidade=$dado['unico'];
    $defeitos_cognex+=[$dia=>$quantidade]; 
}
$defeitos_opencadd=[];
$con = $link->query($consulta_opencadd_def) or die($link->error);
while($dado = $con->fetch_array()) { 
    $dia=$dado['Dia'];
    $quantidade=$dado['unico'];
    $defeitos_opencadd+=[$dia=>$quantidade];
 }

$defeitos_ngene=[];
$con = $link->query($consulta_ngene_def) or die($link->error);
while($dado = $con->fetch_array()) {
    $dia=$dado['Dia'];
    $quantidade=$dado['unico'];
    $defeitos_ngene+=[$dia=>$quantidade]; 
}

$defeitos_dia=[];
$i=0;
foreach ($defeitos_total as $dia=>$total){

    //echo "$dia = Total: $defeitos_total[$dia] - Cognex:$defeitos - Ngene: $defeitos_ngene[$dia] - Opencadd: $defeitos_opencadd[$dia]<br>";
    //$v=array($dia,$defeitos_total[$dia],$defeitos,$defeitos_ngene[$dia],$defeitos_opencadd[$dia]);
    if(empty($defeitos_ngene[$dia])){
        $ngn=0;
    }else{
         $ngn=$defeitos_ngene[$dia];
    }

    if(empty($defeitos_opencadd[$dia])){
        $open=0;
    }else{
        $open=$defeitos_opencadd[$dia];
    }

    if(empty($defeitos_cognex[$dia])){
        $cgx=0;
    }else{
        $cgx=$defeitos_cognex[$dia];
    }

    if($total>0){
        

    $defeitos_dia+=[$i=>array($dia,100,intval(($cgx/$total)*100),intval(($ngn/$total)*100),intval(($open/$total)*100))];
    //print_r($defeitos_dia);
    $i++;
    }else{

    }
}

//print_r($defeitos_cognex);
//print_r($defeitos_ngene);
//print_r($defeitos_dia);


 
#incluindo a classe. verifique se diretorio e versao sao iguais, altere se precisar
include('phplot/phplot.php');

$plot = new PHPlot(1300, 400);
$plot->SetImageBorderType('plain');

$plot->SetPlotType('bars');
$plot->SetDataType('text-data');
$plot->SetDataValues($defeitos_dia);
$plot->SetShading(0);
# Main plot title:
$plot->SetTitle('% Rejeito Região 2 - Cognex - Ngene - Fit -  Ultimos 20 dias');
$plot->SetXTitle('Dia');
$plot->SetYTitle('Quantidade');
# Make a legend for the 3 data sets plotted:

$plot->SetYDataLabelPos('plotin');
# Turn off X tick labels and ticks because they don't apply here:

# Make a legend for the 3 data sets plotted:
$plot->SetLegend(array('% Total', '% Fail -Cognex','% Fail -Ngene', '% Fail-Fit'));
$plot->SetXTickLabelPos('none');
$plot->SetXTickPos('none');
$plot->DrawGraph();


?>