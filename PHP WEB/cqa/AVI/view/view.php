<?php

//class para auxiliar a adição de texto e detalhes nas imagens


class View_Results{

    public $img;
    public $img_true;
    public $dir;
    public $PATH_IMG_DB='D:/automation/cqa/images/raw/';
    public $extension;
    public $colors=[];    //= imagecolorallocate($this->img, 255, 0, 0);   
    public $line=[0=>0,1=>0,2=>0,3=>0];
    

    Function Define_image($img){ 
        $this->extension=explode('.',$img)[1];
        $this->dir=$this->PATH_IMG_DB.$img;
        return $this->extension;        
    }

    Function Creating_img(){        

        if(file_exists($this->dir)){
            if($this->extension=='bmp'){
                $this->img = imagecreatefrombmp($this->dir);  // Load the stamp and the photo
                return "bmp";
            }elseif($this->extension=='png'){
                $this->img = imagecreatefrompng($this->dir);  // Load the stamp and the photo
                return "png";
            }elseif($this->extension=='jpg'){
                $this->img = imagecreatefromjpeg($this->dir);  // Load the stamp and the photo
                return "jpg";
            }
        }else{
            return false;
        }
    }

    Function True_color() {
        $this->img_true = imagecreatetruecolor(imagesx($this->img), imagesy($this->img)); // convert a truecolor
        imagecopy($this->img_true, $this->img, 0, 0, 0, 0, imagesx($this->img), imagesy($this->img)); //copy im
        return true;
    } 


    function add_text($text,$col){  // support only 4 coll - 0 ~3     

        $this->colors = [
            imagecolorallocate($this->img_true, 255, 0, 0), 
            imagecolorallocate($this->img_true, 0, 255, 0),
            imagecolorallocate($this->img_true, 0, 255, 255),   
            imagecolorallocate($this->img_true, 255, 255, 0)
        ];

        if($col<4){        
            $this->line[$col]=$this->line[$col]+1;
            imagettftext($this->img_true,20,0,5+(400*$col),25*$this->line[$col], $this->colors[$col], "arial.ttf", $text );
            return true;
        }else{
            return false;     

        }
    }

    Function show(){

        if($this->extension=="png"){
            header("Content-Type: image/png");  // define header
            imagepng($this->img_true);
            return true;
        }elseif($this->extension=="bmp"){
            header("Content-Type: image/bmp");  // define header
            imagebmp($this->img_true);
            return true;
        }elseif($this->extension=="jpeg"){
            header("Content-Type: image/jpg");  // define header
            imagejpg($this->img_true);
            return true;
        }else{
            return false;
        }
    }
}


$teste = new View_Results;
$teste->Define_image("teste.bmp");
$teste->Creating_img();
$teste->True_color();
$teste->add_text("Resultado Cognex",0);
$teste->add_text("Resultado Manual",1);
$teste->add_text("Resultado Ngene",2);
$teste->add_text("Resultado Fit",3);
$teste->add_text("Segunda linha",0);
$teste->add_text("Segunda linha",1);
$teste->add_text("Segunda linha",1);
$teste->add_text("Segunda linha",1);
$teste->add_text("Segunda linha",1);
$teste->add_text("Segunda linha",3);
$teste->show();

