﻿<head>
<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
-->
<?php
include('style.php');
?>
		
		<script type="text/javascript" language="javascript" src="jquery-3.5.1.js"></script>
		<script type="text/javascript" language="javascript" src="jquery.dataTables.min.js"></script>
		<script type="text/javascript" charset="utf-8">
			

            $.extend( true, $.fn.dataTable.defaults, {
                "searching": true,
                "ordering": true
            } );
            
            
            $(document).ready(function() {
                $('#example').DataTable( {
                "order": [[ 0, "desc" ]]
        
            } );
} );



		</script>

<title>AVI - REPROVAÇÕES</title>
</head>
<body>
<a href="http://mnsnt066/cqa/" id="home"><b> -Home<--</b></a><br>
<br><br>
<?php
$link = new MySQLi('mnsnt066', 'fit_inventario', 'Flex@2k20', 'cqa_v2');
if($link->connect_error){
   echo "Desconectado! Erro: " . $link->connect_error;
}
?>

<?php
//update 22-03-2022 provisorio ate migrar para o BD

$modelos=array(
    'SA78D05862'=>array( 0=>0.77, 1=>0.40, 2=>0.37,  3=>0.34, 4=>0.29, 5=>0.26,  6=>0.25,  7=>0.50,  8=>0.63,  9=>0.48 ),
    'SA78D05864'=>array( 0=>0.50, 1=>0.40, 2=>0.47,  3=>0.64, 4=>0.52, 5=>0.33,  6=>0.43,  7=>0.36,  8=>0.37,  9=>0.32 ),
    'SA78D21897'=>array( 0=>0.85, 1=>0.73, 2=>0.42,  3=>0.78, 4=>0.40, 5=>0.64,  6=>0.55,  7=>0.51,  8=>0.61,  9=>0.65 ),
    'SA78D21898'=>array( 0=>0.50, 1=>0.40, 2=>0.62,  3=>0.46, 4=>0.36, 5=>0.64,  6=>0.47,  7=>0.63,  8=>0.63,  9=>0.53 ),
    'SA78D21899'=>array( 0=>0.50, 1=>0.40, 2=>0.88,  3=>0.97, 4=>0.50, 5=>0.63,  6=>0.41,  7=>0.53,  8=>0.49,  9=>0.64 ),
    'SA78D22661'=>array( 0=>0.85, 1=>0.73, 2=>0.42,  3=>0.78, 4=>0.40, 5=>0.64,  6=>0.55,  7=>0.51,  8=>0.61,  9=>0.65 )
);

//fim modelos

   
$consulta = "SELECT * FROM inspected_images where result=1 ORDER BY uid DESC LIMIT 1000"; 

$con = $link->query($consulta) or die($link->error);
?>
<div id="di">
<table id="example" class="display" style="width:100%">
    <thead>
        <tr>
            <th>UID</th>
            <th>TRACK ID</th>
            <th>REGION ID</th>
            <th>REGION</th>
            <th>IA TIME(ms)</th>
            <th>MODEL</th>
            <th>SCORE</th>
            <th>LIMIT</th>
            <th>FILENAME</th>
            <th>MASCARA</th>
            <th>DATE</th>              
            
        </tr>
    </thead>

    <tbody>
        
            
<?php

while($dado = $con->fetch_array()) {

    $consulta2 = "SELECT * FROM registered_regions where region_id='$dado[id_region]' ORDER BY uid DESC LIMIT 1000"; 
                    $con2 = $link->query($consulta2) or die($link->error);
                    while($dado2 = $con2->fetch_array()) {
                    $parts_name=explode("_",$dado2['filename']);
                    $region=explode(".",$parts_name[3])[0];

                    $consulta3 = "SELECT * FROM registered_products where uid='$parts_name[0]' ORDER BY uid DESC LIMIT 1"; 
                    $con3 = $link->query($consulta3) or die($link->error);
                    while($dado3 = $con3->fetch_array()) {
                        $model=$dado3['model'];
                    }                        

                    echo "<tr>";
                    echo "<td>";
                        echo $parts_name[0];
                    echo "</td>";
                    echo "<td>";

                    echo "<form action=\"http://mnsnt066.americas.ad.flextronics.com/cqa/unit_info/\" method=\"POST\">
                        <input type=\"hidden\" name=\"track_id\" value=\"".$parts_name[1]."\"/>
                        <input type=\"submit\" value=\"".$parts_name[1]."\" />
                    </form>";
                        
                    echo "</td>";
                    echo "<td>";
                        echo $dado2['region_id']."</td>";
                    echo "<td>";
                        echo $region[0]."</td>";
                    echo "<td>";
                    echo $dado['processing_time']."</td>";
                    echo "<td>";
                        echo $model."</td>";
                    echo "<td>";
                    echo $dado['classification_score']."</td>";
                    echo "<td>";
                    echo $dado['limit_score']."</td>";           
                    echo "<td>";
                        echo "<a href=http://mnsnt066/cqa/file_info.php?file=".$dado2['filename'].">".$dado2['filename']."</a>";
                    echo "</td>";
                    echo "<td>";
                        echo "<a href=http://mnsnt066/cqa/config/graphics_exclusion.php?file=".$dado2['filename'].">Verificar Mascaras</a>"; 
                    echo "</td>";                    
                    echo "<td>";
                        echo $dado2['date'];
                    echo "</td>";                 
                    
                    echo "</tr>";
                    }

}

?>

</tbody>
</table>
</div>
</body>
</html>

