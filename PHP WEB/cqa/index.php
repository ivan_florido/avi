<!DOCTYPE html>
<html>
  <head>
    <title>Avi Project - FIT</title>

    <style>
        a:link {
          background-color: white; color: black; text-decoration: none; font-family:courier,arial,helvetica;
        }

        a:visited {
          background-color: white; color: black; font-size:15px; text-decoration: none; font-family:courier,arial,helvetica;
        }

        a:hover {
          background-color: lightgreen; color: blue; font-size:20px; text-decoration: none;font-family:courier,arial,helvetica;
        }

        a:active {
          background-color: red; text-decoration: none;font-family:courier,arial,helvetica;;
        }

        #titulo{
          color: DimGrey; font-size:40px; font-family:courier,arial,helvetica;
        }
        #result{
          color: blue; font-size:15px;
        }
        #consulta{
          color: black; font-size:15px
        }

        #retangulo{
          background-color: white; width: 100%; height: 50px; border-radius: 5px;border-color: black ;

        }

        #graficos{
          background-color: white; width: 100%; height: 30%; border-radius: 5px; border-color: black; border-spacing: 5px ; 
        }
        #main{
          background-color: black; width: 1200px; height: 100%; border-radius: 5px; border-color: black; border-spacing: 5px ; 
        }

        #menu{
          background-color: white; width: 100%; height: 100%; border-radius: 5px; border-color: black; border-spacing: 5px ; 
        }

        #rodape{
          background-color: white; width: 100%; height: 100%; border-radius: 5px; border-color: black; border-spacing: 5px ; 
        }

    </style>

  </head>

  <body bgcolor="black" width=800px>
    <div id="main">
      <div id="retangulo">
        <div id="titulo"><center><b>FIT - AVI PROJECT - INFORMATIONS</b></center></div>
      </div>
      <br>
      <div id="graficos">
        <br>
        <center><img src="http://mnsnt066.americas.ad.flextronics.com/cqa/pareto_hh.php">    <img src="http://mnsnt066.americas.ad.flextronics.com/cqa/pie_day.php"></center><br>
        <center><img src="http://mnsnt066.americas.ad.flextronics.com/cqa/pareto.php"></center>
        <div id="result">

          <?php
          $url="http://mnsnt066.americas.ad.flextronics.com/cqa/limit_nok.php?limit=100";
          $n_100= @file_get_contents($url);
          echo "&nbsp;&nbsp;&nbsp;&nbsp;Nos ultimos 100 foram reprovados:".$n_100;
          $df_c = intval(((disk_free_space("D:")/1024)/1024)/1024);
          $ds = intval(((disk_total_space("D:")/1024)/1024)/1024);
          $ciclos=intval((intval(((disk_free_space("D:")/1024)/1024)/1024)) / 0.131);
          echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;O espaço disponivel em disco para imagens é de $df_c GB de $ds GB totais";
          echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;O banco de dados de imagem suporta aproximadamente: $ciclos telefones";
          ?>
        </div>
      </div>
      <br>
      <div id="menu">
        <br>
        <ol>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/reprovacoes/">&nbsp;Lista da Ultimas imagens reprovadas da Maquina de AVI</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/registra_log.php">&nbsp;Lista de Erros Registrados</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/registra_exclusao.php">&nbsp;Lista de Eliminados por Filtro de Posição</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/registra_gap.php">&nbsp;Lista dos enviados para o servidor analitico de gap</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/config/">&nbsp;Configurar Receitas</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/config.php">&nbsp;Configurar parametros globais da Maquina</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/unit_info/">&nbsp;Consulta Unit History</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/producao.php">&nbsp;Consulta Produção - Governance</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/dashboard_cqa/">&nbsp;Dashboard Marcação Manual de defeitos</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/registra_mask.php">&nbsp;Acompanhamento Mascaras Dinamicas</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/dashboard.html">&nbsp;ElasticSearch Dashboard</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/server.html">&nbsp;ElasticSearch Server Information Dashboard</a></li>
          <li><a href="http://mnsnt066.americas.ad.flextronics.com/cqa/results_r2.php">&nbsp;Comparativo Reprovações Cognex - Ngene - Fit</a></li>
        </ol>
        <div id="consulta">Consultar uma Imagem</div>
        <div id="consulta">(ex: 63798_ZF523KLD2J_2021-10-28_7.bmp)</div>
        <form action="http://mnsnt066.americas.ad.flextronics.com/cqa/file_info.php" method="get">
          <input type="text" value="*.bmp ou *.png " name="file" width="200">
          <button type="submit">Consultar Imagem</button> 
        </form>
        <br>
      </div>
      <br>
      <div id="rodape">
      <center>FIT - Instituto De Tecnologia Da Amazonia em Manaus-AM</center>
      </div>
    </div>
  <body>
<html>