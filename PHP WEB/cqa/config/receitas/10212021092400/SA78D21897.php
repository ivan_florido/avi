<?php

//*************************************************************************************************************************/
//******************inicio areas de exclusão ******************************************************************************/
//**Model:SA78D05862             */
//**Model Name: JAVA FLAMINGO    */
//**Data: 10-18-2021             */

$r0=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>0,"end_y"=>0)     
);

$r1=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>0,"end_y"=>0)     
);

$r2=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>0,"end_y"=>0)     
);

$r3=array(
    1=>array("ini_x"=>2440,"ini_y"=>280,"end_x"=>2490,"end_y"=>320),   //1 ponto add  onix black
    2=>array("ini_x"=>4588,"ini_y"=>4067,"end_x"=>4720,"end_y"=>4174),
    3=>array("ini_x"=>4144,"ini_y"=>4274,"end_x"=>4277,"end_y"=>4383),
    4=>array("ini_x"=>3548,"ini_y"=>0,"end_x"=>3737,"end_y"=>350), 
    5=>array("ini_x"=>2290,"ini_y"=>0,"end_x"=>3190,"end_y"=>315),
    6=>array("ini_x"=>4522,"ini_y"=>4076,"end_x"=>4656,"end_y"=>4198)
);

$r4=array(
    1=>array("ini_x"=>0,"ini_y"=>645,"end_x"=>5328,"end_y"=>750),
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>360,"end_y"=>750)     
);

$r5=array(
    1=>array("ini_x"=>0,"ini_y"=>645,"end_x"=>5328,"end_y"=>750), //1 ponto add
    2=>array("ini_x"=>990,"ini_y"=>0,"end_x"=>2496,"end_y"=>145) //1 ponto add
);

$r6=array(
    1=>array("ini_x"=>0,"ini_y"=>691,"end_x"=>5328,"end_y"=>750)     
);

$r7=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>150,"end_y"=>750),
    2=>array("ini_x"=>0,"ini_y"=>588,"end_x"=>5328,"end_y"=>750)             
);

$r8=array(
    1=>array("ini_x"=>0,"ini_y"=>608,"end_x"=>5328,"end_y"=>750)     
);

$r9=array(
    1=>array("ini_x"=>0,"ini_y"=>702,"end_x"=>5328,"end_y"=>750), //1 ponto add)    // area para o grafite 
    2=>array("ini_x"=>2342,"ini_y"=>0,"end_x"=>3038,"end_y"=>568) //1 ponto add)
);

/********************************************inicio config coordenadas do GAP*****************************/

$gap_region_4=array("limit_down"=>500,"limit_up"=>550);
$gap_region_5=array("limit_down"=>500,"limit_up"=>550);
$gap_region_6=array("limit_down"=>550,"limit_up"=>605);
$gap_region_7=array("limit_down"=>496,"limit_up"=>530);
$gap_region_8=array("limit_down"=>475,"limit_up"=>515);
$gap_region_9=array("limit_down"=>580,"limit_up"=>580);
        

?>