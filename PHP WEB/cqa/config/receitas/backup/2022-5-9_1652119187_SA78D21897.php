<?php

//*************************************************************************************************************************/
//******************inicio areas de exclusão ******************************************************************************/
//**Model:SA78D05661             */
//**Model Name: JAVA ONIX BLACK    */
//**Data: 28-01-2021             */

$id_IA=17; //* ID da versão da rede utilizada */


$r0=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>234),     //ponto padrao
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>513,"end_y"=>4608),     //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>4473,"end_x"=>5328,"end_y"=>4608)  //ponto padrao   
);

$r1=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>234),     //ponto padrao
    2=>array("ini_x"=>4698,"ini_y"=>0,"end_x"=>5328,"end_y"=>4608), //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>4393,"end_x"=>5328,"end_y"=>4608)  //ponto padrao   
);

$r2=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>260),     //ponto padrao
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>414,"end_y"=>4608),     //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>4429,"end_x"=>5328,"end_y"=>4608)  //ponto padrao
    ,4=>array("ini_x"=>2579,"ini_y"=>3866,"end_x"=>2690,"end_y"=>4136)  //ponto padrao   

);


$r3=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>279),           //ponto padrao
    2=>array("ini_x"=>4897,"ini_y"=>0,"end_x"=>5328,"end_y"=>4608),       //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>4428,"end_x"=>5328,"end_y"=>4608)       //ponto padrao
    

);

$r4=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>198),     //ponto padrao
    2=>array("ini_x"=>4660,"ini_y"=>0,"end_x"=>5328,"end_y"=>750),  //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>630,"end_x"=>5328,"end_y"=>750),   //ponto padrao
    4=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>456,"end_y"=>750)     //ponto padrao   
);

$r5=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>200),      //ponto padrao
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>440,"end_y"=>750),       //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>630,"end_x"=>5328,"end_y"=>750)     //ponto padrao
);

$r6=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>150),      //ponto padrao
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>550,"end_y"=>750),       //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>660,"end_x"=>5328,"end_y"=>750),      //ponto padrao  
    4=>array("ini_x"=>4780,"ini_y"=>0,"end_x"=>5328,"end_y"=>750)    //ponto padrao      
);

$r7=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>200),     //ponto padrao
    2=>array("ini_x"=>4848,"ini_y"=>0,"end_x"=>5328,"end_y"=>750),  //ponto padrao 
    3=>array("ini_x"=>0,"ini_y"=>590,"end_x"=>5328,"end_y"=>750),   //ponto padrao
    4=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>300,"end_y"=>750),   
    5=>array("ini_x"=>600,"ini_y"=>124,"end_x"=>670,"end_y"=>196),
    6=>array("ini_x"=>560,"ini_y"=>100,"end_x"=>795,"end_y"=>178),
    7=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>450,"end_y"=>218),
    8=>array("ini_x"=>0,"ini_y"=>570,"end_x"=>730,"end_y"=>750),
    9=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>200),
    10=>array("ini_x"=>0,"ini_y"=>558,"end_x"=>310,"end_y"=>598),
    11=>array("ini_x"=>4751,"ini_y"=>484,"end_x"=>4844,"end_y"=>579)                  
);

$r8=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>165),  //ponto padrao
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>854,"end_y"=>750),   //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>590,"end_x"=>5328,"end_y"=>750), //ponto padrao  
    4=>array("ini_x"=>1027,"ini_y"=>464,"end_x"=>1067,"end_y"=>514) //ponto padrao 
       
);

$r9=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>165),        //ponto padrao   
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>718,"end_y"=>750),         //ponto padrao
    3=>array("ini_x"=>4632,"ini_y"=>0,"end_x"=>5328,"end_y"=>750),     //ponto padrao
    4=>array("ini_x"=>0,"ini_y"=>665,"end_x"=>5328,"end_y"=>750),      //ponto padrao
    5=>array("ini_x"=>2332,"ini_y"=>0,"end_x"=>3038,"end_y"=>568)      //1 ponto add)
);

/********************************************inicio config coordenadas do GAP*****************************/

$gap_region_4=array("limit_down"=>488,"limit_up"=>528);
$gap_region_5=array("limit_down"=>520,"limit_up"=>560);
$gap_region_6=array("limit_down"=>565,"limit_up"=>610);
$gap_region_7=array("limit_down"=>480,"limit_up"=>520);
$gap_region_8=array("limit_down"=>472,"limit_up"=>510);
$gap_region_9=array("limit_down"=>565,"limit_up"=>595);
        
?>