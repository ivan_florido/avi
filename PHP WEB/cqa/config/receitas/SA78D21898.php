<?php

//*************************************************************************************************************************/
//******************inicio areas de exclusão ******************************************************************************/
//**Model:SA78D05898                   */
//**Model Name: JAVA TEAL CHAMALEON    */
//**Data: 11-09-2021                   */

$id_IA=54; //* ID da versão da rede utilizada */

$r0=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>234),     //ponto padrao
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>513,"end_y"=>4608),     //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>4473,"end_x"=>5328,"end_y"=>4608)  //ponto padrao     
);

$r1=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>234),     //ponto padrao
    2=>array("ini_x"=>4698,"ini_y"=>0,"end_x"=>5328,"end_y"=>4608), //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>4393,"end_x"=>5328,"end_y"=>4608)  //ponto padrao      
);

$r2=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>260),     //ponto padrao
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>414,"end_y"=>4608),     //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>4429,"end_x"=>5328,"end_y"=>4608)  //ponto padrao     
);

$r3=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>279),           //ponto padrao
    2=>array("ini_x"=>4897,"ini_y"=>0,"end_x"=>5328,"end_y"=>4608),       //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>4428,"end_x"=>5328,"end_y"=>4608)       //ponto padrao
  

);

$r4=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>198),     //ponto padrao
    2=>array("ini_x"=>4660,"ini_y"=>0,"end_x"=>5328,"end_y"=>750),  //pomto padrao
    3=>array("ini_x"=>0,"ini_y"=>600,"end_x"=>5328,"end_y"=>750),   //ponto padrao
    4=>array("ini_x"=>0,"ini_y"=>575,"end_x"=>617,"end_y"=>750),
    5=>array("ini_x"=>0,"ini_y"=>527,"end_x"=>545,"end_y"=>547)     //falso positivo      
);

$r5=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>200),      //ponto padrao
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>460,"end_y"=>750),       //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>624,"end_x"=>5328,"end_y"=>750),     //ponto padrao
    4=>array("ini_x"=>1000,"ini_y"=>44,"end_x"=>2544,"end_y"=>160)   //1 ponto add
);

$r6=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>150),      //ponto padrao
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>630,"end_y"=>750),       //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>660,"end_x"=>5328,"end_y"=>750),      //ponto padrao  
    4=>array("ini_x"=>4800,"ini_y"=>0,"end_x"=>5328,"end_y"=>750)    //ponto padrao      
);

$r7=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>200),     //ponto padrao
    2=>array("ini_x"=>4848,"ini_y"=>0,"end_x"=>5328,"end_y"=>750),  //ponto padrao 
    3=>array("ini_x"=>0,"ini_y"=>590,"end_x"=>5328,"end_y"=>750)   //ponto padrao
          
);

$r8=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>165),  //ponto padrao
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>854,"end_y"=>750),   //ponto padrao
    3=>array("ini_x"=>0,"ini_y"=>590,"end_x"=>5328,"end_y"=>750) //ponto padrao            
);

$r9=array(
    1=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>5328,"end_y"=>195),        //ponto padrao   
    2=>array("ini_x"=>0,"ini_y"=>0,"end_x"=>728,"end_y"=>750),         //ponto padrao
    3=>array("ini_x"=>4632,"ini_y"=>0,"end_x"=>5328,"end_y"=>750),     //ponto padrao
    4=>array("ini_x"=>0,"ini_y"=>660,"end_x"=>5328,"end_y"=>750)      //ponto padrao
  
);

/********************************************inicio config coordenadas do GAP*****************************/

$gap_region_4=array("limit_down"=>488,"limit_up"=>528);
$gap_region_5=array("limit_down"=>520,"limit_up"=>560);
$gap_region_6=array("limit_down"=>565,"limit_up"=>610);
$gap_region_7=array("limit_down"=>480,"limit_up"=>520);
$gap_region_8=array("limit_down"=>472,"limit_up"=>510);
$gap_region_9=array("limit_down"=>565,"limit_up"=>595);
        

?>