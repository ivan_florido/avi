<?php
/*************************************************************************************************************/
// Inicio mascara dinamica para etiqueta
// Este Bloco tem por objetivo receber as coordenada obtidas pela estação para a etiqueta traseira
// e atualziar a mascara de posiveis defeitos
// reduzindo o numero de erros por posição incorreta da etiqueta traseira
//***********************************************************************************************************/ 

$regiao="3";  // região onde ira ser verificado
$centro_x=10; // parametro result da IA
$centro_y=10; // parametro resulta da IA

$fator_compensador_x=2.6; // valor para compensar as coordenadas em X obtidas na entrada da maquina
$fator_compensador_y=1.7; // valor para compensar as coordenadas em Y obtidas na entrada da maquina
$soma_x=-255;
$soma_y=-300;

if(!empty($_GET["filename"]) && !empty($_GET["p_x"]) && !empty($_GET["p_y"]) && !empty($_GET["a"]) && $regiao=="3" ){ // check if  all conditions is OK
    
    $img=$_GET['filename'];    
    $local_file="D:/automation/cqa/images/raw/".$img;
    $cx=$_GET["p_x"];
    $cy=$_GET["p_y"];
    $c_angle=$_GET["a"];

    //tamanho da etiqueta em pixels
    $delta_x=2700;
    $delta_y=2700;
    
    if($c_angle > 300){

        $c_angle= 360 - $c_angle;

    }
    if($c_angle<10){
        $c_angle= 6;
    }
    if($c_angle>10){
        $c_angle= 12;
    }

    //incremento no tamanho da etiqueta
    $offset=200*($c_angle/6);
    
    $c_x_compensado=intval(($cx+$soma_x)*$fator_compensador_x);
    $c_y_compensado=intval(($cy+$soma_y)*$fator_compensador_y);


    $x0=$c_x_compensado-$offset;
    $y0=$c_y_compensado-$offset;

    $x1=$c_x_compensado+$delta_x+$offset;
    $y1=$c_y_compensado+$delta_y+$offset;


    //verifica se o defeito esta dentro da area da etiqueta
    if($centro_x>$x0 and $centro_x<$x1 and $centro_y>$y0 and $centro_y<$y1){
                                    
        $reg_exclusao="http://mnsnt066/cqa/registra_exclusao_dinamica.php?filename=".$_GET['filename']."&result=0&cx=".$cx."&cy=".$cy."&centro_x=".$centro_x."&centro_y=".$centro_y."&status=done";
        @file_get_contents($reg_exclusao);

        $area=0;
        $x0=0;
        $x1=0;
        $y0=0;
        $y1=0;
        $centro_x=0;
        $centro_y=0;
        $largura=0;
        $altura=0;
    
    }

    //area visual para testes 
    header("Content-Type: image/bmp");  // define header
    $im = imagecreatefrombmp($local_file);   
    $red = imagecolorallocate($im, 255, 0, 0);   
    imagerectangle($im, $x0,$y0,$x1,$y1, $red);         
    imagebmp($im); 
    //area visual

}else{
    //4=>array("ini_x"=>1740,"ini_y"=>800,"end_x"=>5328,"end_y"=>3800)      //1 ponto add  onix black

    //default
    $x0=1740;
    $y0=800;
    $x1=5328;
    $y1=3800;


    if($centro_x>$x0 and $centro_x<$x1 and $centro_y>$y0 and $centro_y<$y1){
                                    
        $reg_exclusao="http://mnsnt066/cqa/registra_exclusao_dinamica.php?filename=".$_GET['filename']."&result=0&centro_x=".$centro_x."&centro_y=".$centro_y."&status=done";
        @file_get_contents($reg_exclusao);

        $area=0;
        $x0=0;
        $x1=0;
        $y0=0;
        $y1=0;
        $centro_x=0;
        $centro_y=0;
        $largura=0;
        $altura=0;
    
    }




    $erro="http://mnsnt066/cqa/registra_exclusao_dinamica.php?filename=".$_GET['filename']."&result=0&centro_x=".$centro_x."&centro_y=".$centro_y."&status=default";
    @file_get_contents($erro);
    $img=$_GET['filename'];    
    $local_file="D:/automation/cqa/images/raw/".$img;
    header("Content-Type: image/bmp");  // define header
    $im = imagecreatefrombmp($local_file);   
    $red = imagecolorallocate($im, 255, 0, 0);   
    imagerectangle($im, $x0,$y0,$x1,$y1, $red);         
    imagebmp($im); 
    //area visual


}
// FIM mascara dinamica para etiqueta
//**********************************************************************************************************************/

?>