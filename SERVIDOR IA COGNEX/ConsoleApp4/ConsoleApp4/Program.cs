﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using ViDi2;
using ViDi2.Local;
using System.IO;
using System.Windows.Forms;
using ConsoleApp4;


public class CognexServer
{

    private const int portNum = 13;   //port to recieved the img name 
    private const int portmodel = 15; // port to change ia model

    [STAThread]

    public static int Main(String[] args)
    {
         

        //Application.EnableVisualStyles();
        
        //Application.SetCompatibleTextRenderingDefault(false);
        //Application.Run(new Form1());
        
        

        bool done = false;

        //tcp listene initialize
        var ia_change = new TcpListener(IPAddress.Any, portmodel);

        ia_change.Start();
        //Console.Write("\nRede Inicializada\n\n");
        // main loop started
        while (!done)
        {

            string x;
            string resultado;


            Console.ForegroundColor = ConsoleColor.Yellow;
            // TCP start
            Console.Write("\nWaiting for connection IA...");
            TcpClient client1 = ia_change.AcceptTcpClient();

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            //recieved
            Console.WriteLine("IA model recieved.");
            NetworkStream ns = client1.GetStream();

            Console.ForegroundColor = ConsoleColor.Gray;


            if (ns.CanRead)
            {


                byte[] myReadBuffer = new byte[1024];
                StringBuilder rede = new StringBuilder();
                int numberOfBytesRead = 0;

                // Incoming message may be larger than the buffer size.
                do
                {
                    numberOfBytesRead = ns.Read(myReadBuffer, 0, myReadBuffer.Length);

                    rede.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));
                }

                while (ns.DataAvailable);

                Console.Write("\nIniciando Rede:\n\n");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("C:\\IA_modelos\\" + rede+ ".vrws\n\n");

                if (File.Exists("C:\\IA_modelos\\" + rede+".vrws")){

                    
                    

                    ViDi2.Runtime.Local.Control control = new ViDi2.Runtime.Local.Control(GpuMode.SingleDevicePerTool);
                    //Initializes all CUDA devices
                    control.InitializeComputeDevices(GpuMode.SingleDevicePerTool, new List<int>() { });

                    // Open a runtime workspace from file
                    // the path to this file relative to the example root folder
                    // and assumes the resource archive was extracted there.
                    ViDi2.Runtime.IWorkspace workspace = control.Workspaces.Add("workspace", "C:\\IA_modelos\\" + rede+".vrws");


                    // Store a reference to the stream 'default'
                    IStream stream = workspace.Streams["default"];

                    //define initialize variable done                


                    bool done2 = false;

                    //tcp listene initialize
                    var listener = new TcpListener(IPAddress.Any, portNum);

                    listener.Start();
                    Console.Write("\nRede Inicializada\n\n");
                    // main loop started

                    x = "1\r\n";
                    byte[] byteTime2 = Encoding.ASCII.GetBytes(x);

                    ns.Write(byteTime2, 0, byteTime2.Length);
                    //Console.Write("aqui 2");
                    ns.Close();
                    //Console.Write("aqui 3");
                    client1.Close();
                    //Console.Write("aqui 4");

                    while (!done2)
                    {


                        Console.ForegroundColor = ConsoleColor.Yellow;
                        // TCP start
                        Console.Write("Waiting for connection...");
                        TcpClient client = listener.AcceptTcpClient();

                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        //recieved
                        Console.WriteLine("Connection accepted.");
                        NetworkStream ns2 = client.GetStream();

                        Console.ForegroundColor = ConsoleColor.Gray;


                        if (ns2.CanRead)
                        {

                            //byte[] myReadBuffer = new byte[1024];
                            StringBuilder myCompleteMessage = new StringBuilder();
                            //int numberOfBytesRead = 0;

                            // Incoming message may be larger than the buffer size.
                            do
                            {
                                numberOfBytesRead = ns2.Read(myReadBuffer, 0, myReadBuffer.Length);

                                myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead));
                            }

                            while (ns2.DataAvailable);


                            //IStream stream = workspace.Streams["default"];

                            if (File.Exists("X:\\" + myCompleteMessage)) // check if the filename is valid
                            {

                                
                                IImage image = new LibraryImage("X:\\" + myCompleteMessage); // open image from x:\\


                                ISample sample = stream.CreateSample(image);


                                // We can process the red tool, it won't reprocess the blue tool since the result
                                // is up to date.

                                //define some variables
                                string result, regiao;
                                int regiao_pos;
                                char[] arr;

                                //extract region from filename

                                result = myCompleteMessage.ToString();
                                string[] subs = result.Split('_');
                                arr = subs[3].ToCharArray(0, 1);
                                regiao = arr[0].ToString();



                                // Console.WriteLine(regiao);
                                regiao_pos = Int32.Parse(regiao.PadLeft(1)); //
                                Console.WriteLine("Regiao:" + regiao.PadLeft(1));

                                //check the region and select the correct tool to use in analize

                                //redirect file to correct steam tool analize

                                ITool r0 = stream.Tools["R" + regiao_pos];  //display

                                sample.Process(r0);
                                IRedMarking redMarking = sample.Markings[r0.Name] as IRedMarking;
                                Console.WriteLine("R" + regiao_pos);

                                foreach (IRedView view in redMarking.Views)
                                {
                                    Console.WriteLine($"UUID:{r0.Uuid.ToString()}");

                                    Console.WriteLine("Arquivo: " + myCompleteMessage);
                                    System.Console.WriteLine($"Resultado {view.Score}");

                                    //view.HeatMap.Bitmap.Save("V:\\" + myCompleteMessage); // save heat map


                                    x = "";
                                    resultado = "";

                                    foreach (IRegion region in view.Regions)
                                    {
                                        x += "Score:" + region.Score + "-Area:" + region.Area + "-Centro X:" + region.Center.X + "-Centro Y:" + region.Center.Y + "-Altura:" + region.Height + "-Largura:" + region.Width + "-Limite:"+view.Threshold.Lower+ "-uuid:" + r0.Uuid + ";";
                                        resultado += "Score:" + region.Score + ",Area:" + region.Area + ",Centro X:" + region.Center.X + ",Centro Y:" + region.Center.Y + ",Altura:" + region.Height + ",Largura:" + region.Width + ",Limite:" + view.Threshold.Lower + "-uuid:" + r0.Uuid + "\n";
                                    }

                                    if (x == "")
                                    {

                                        x = "Score:" + view.Score + "-Area:0-Centro X:0-Centro Y:0-Altura:0-Largura:0-Limite:"+view.Threshold.Lower+ "-uuid:" + r0.Uuid + ";";
                                        resultado = "Score:" + view.Score + "-Area:0-Centro X:0-Centro Y:0-Altura:0-Largura:0-Limite:"+view.Threshold.Lower+ ",uuid:" + r0.Uuid + "\n";
                                        x = x + "\r\n";
                                        Console.WriteLine($"Resultado da Avaliacao:");
                                        Console.WriteLine($"{resultado}\n");

                                        if (regiao_pos == 1 || regiao_pos == 2)
                                        {

                                            ITool rede_aux = stream.Tools["C" + regiao_pos];  //display
                                            sample.Process(rede_aux);
                                            IRedMarking redMarking2 = sample.Markings[rede_aux.Name] as IRedMarking;
                                            Console.WriteLine("Processamento auxiliar C" + regiao_pos);

                                            foreach (IRedView view2 in redMarking2.Views)
                                            {
                                                //Console.WriteLine($"Limite:{view2.Threshold.Lower.ToString()}");
                                                Console.WriteLine($"UUID:{rede_aux.Uuid.ToString()}");
                                                Console.WriteLine("Arquivo: " + myCompleteMessage);
                                                System.Console.WriteLine($"Resultado {view2.Score}");

                                                //view2.HeatMap.Bitmap.Save("V:\\" + "C"+regiao_pos+"_"+myCompleteMessage);

                                                x = "";

                                                foreach (IRegion region2 in view2.Regions)
                                                {
                                                    x += "Score:" + region2.Score + "-Area:" + region2.Area + "-Centro X:" + region2.Center.X + "-Centro Y:" + region2.Center.Y + "-Altura:" + region2.Height + "-Largura:" + region2.Width + "-Limite:" + view.Threshold.Lower + "-uuid:" + r0.Uuid + ";";
                                                    resultado += "Score:" + region2.Score + ",Area:" + region2.Area + ",Centro X:" + region2.Center.X + ",Centro Y:" + region2.Center.Y + ",Altura:" + region2.Height + ",Largura:" + region2.Width + ",Limite" + view.Threshold.Lower + ",uuid:" + r0.Uuid + "\n";
                                                }
                                                if (x == "")
                                                {

                                                    x = "Score:" + view.Score + "-Area:0-Centro X:0-Centro Y:0-Altura:0-Largura:0-Limite:"+view.Threshold.Lower+ "-uuid:" + r0.Uuid + ";";
                                                    resultado = "Score:" + view.Score + "-Area:0-Centro X:0-Centro Y:0-Altura:0-Largura:0--Limite:" + view.Threshold.Lower + ",uuid:" + r0.Uuid + "\n";

                                                }
                                                x = x + "\r\n";



                                            }
                                        }



                                        Console.WriteLine($"Resultado da Avaliacao:");
                                        Console.WriteLine($"{resultado}\n");
                                        
                                      
                                        

                                        byte[] byteTime = Encoding.ASCII.GetBytes(x);

                                        try
                                        {
                                            ns2.Write(byteTime, 0, byteTime.Length);
                                            ns2.Close();
                                            client.Close();
                                        }
                                        catch (System.Exception e)
                                        {
                                            Console.WriteLine(e.ToString());
                                        }
                                    }
                                    else
                                    {

                                        Console.WriteLine($"Resultado da Avaliacao:");
                                        Console.WriteLine($"{resultado}");

                                        x = x + "\r\n";

                                        byte[] byteTime = Encoding.ASCII.GetBytes(x);

                                        try
                                        {
                                            ns2.Write(byteTime, 0, byteTime.Length);
                                            ns2.Close();
                                            client.Close();
                                        }
                                        catch (System.Exception e)
                                        {
                                            Console.WriteLine(e.ToString());
                                        }


                                    }
                                    image.Dispose();
                                    sample.Dispose();

                                }

                           

                            }
                            else // if filename not is valid return -1 to client
                            {  

                                //Console.Write("aqui");
                                string y = "-1";
                                
                                x = y + "\r\n";
                                byte[] byteTime = Encoding.ASCII.GetBytes(x);

                                try
                                {
                                    ns2.Write(byteTime, 0, byteTime.Length);
                                    //Console.Write("aqui 2");
                                    ns2.Close();
                                    //Console.Write("aqui 3");
                                    client.Close();
                                    //Console.Write("aqui 4");
                                }
                                catch (System.Exception e)
                                {
                                    //Console.Write("aqui 5");
                                    Console.WriteLine(e.ToString());
                                    //Console.Write("aqui 6");
                                }

                                //Console.Write("aqui 7");
                                listener.Stop();
                                done2 = true;
                                //return 0;
                                
                                control.Dispose();
                               


                            }

                        }
                        else
                        {
                            Console.WriteLine("Sorry.  You cannot read from this NetworkStream.");
                        }
                    }
                    listener.Stop();
                    //done2 = true;
                    


                    //return 0;
                }
                else // if filename not is valid return -1 to client
                {

                    string y = "-1";
                    x = y + "\r\n";
                    byte[] byteTime = Encoding.ASCII.GetBytes(x);

                    try
                    {
                        ns.Write(byteTime, 0, byteTime.Length);
                        ns.Close();
                        client1.Close();
                    }
                    catch (System.Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }

                }


            }
        }
        ia_change.Stop();
        return 0;
    } 
}
